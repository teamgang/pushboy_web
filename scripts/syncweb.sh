#!/bin/sh
cd ..
flutter build web
cd build/web
rsync -avcPz \
	--delete \
	* pushboi:/papi/web
