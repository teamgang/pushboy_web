import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:flutter/material.dart';
import 'package:flutter_web_plugins/flutter_web_plugins.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/app.dart';

PbConf defaultConf = PbConf(
  firebaseToken: '',
  mockAutoSignIn: true,
  userType: UserType.developer,
  rootDir: '',
);
King defaultKing = King(conf: defaultConf);

void main() async {
  if (kIsWeb) {
    setUrlStrategy(PathUrlStrategy());
  }

  await defaultKing.initAsyncObjects();
  runApp(MyApp(king: defaultKing));
}
