import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class AlertsPage extends StatelessWidget {
  const AlertsPage();

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Center(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const SizedBox(height: 24),
            Text(
              'Alerts',
              style: Theme.of(context).textTheme.headline4,
            ),
            const SizedBox(height: 24),
            const Text('Alerts for developer here.'),
            const SizedBox(height: 24),
            TextButton(
                onPressed: () {
                  Navigator.of(context)
                      .pushNamed(Routes.developer, arguments: const Args());
                },
                child: const Text('Sign in or create an account')),
          ],
        ),
      ),
    );
  }
}
