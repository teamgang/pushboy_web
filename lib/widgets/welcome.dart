import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class Welcome extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Center(
        child: Column(
          //mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            const SizedBox(height: 24),
            Text(
              'Welcome to PushBoi',
              style: Theme.of(context).textTheme.headline4,
            ),
            const SizedBox(height: 24),
            const Text(
                'Send messages and notifications directly to your followers via the PushBoi mobile app.'),
            const SizedBox(height: 24),
            TextButton(
              onPressed: () {
                Navigator.of(context).pushNamed(Routes.signIn);
              },
              child: const Text('Sign in or create an account'),
            ),
          ],
        ),
      ),
    );
  }
}
