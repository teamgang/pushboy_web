import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ConsoleWrapper extends StatefulWidget {
  const ConsoleWrapper({required this.child, required this.initialTab});
  final Widget child;
  final int initialTab;

  @override
  ConsoleWrapperState createState() => ConsoleWrapperState();
}

class ConsoleWrapperState extends State<ConsoleWrapper> {
  //late final ScrollController _scrollController;

  @override
  initState() {
    super.initState();
    //_scrollController = ScrollController();
  }

  @override
  dispose() {
    //_scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const SizedBox(height: 20),
          Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 20),
              TextButton(
                  child: Text18('All projects',
                      style: widget.initialTab == 0
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.console);
                  }),
              const SizedBox(height: 20),
              TextButton(
                  child: Text18('Alerts',
                      style: widget.initialTab == 1
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.alerts);
                  }),
            ],
          ),
          const SizedBox(width: 10),
          Expanded(
            child: Scrollbar(
              //controller: _scrollController,
              thumbVisibility: true,
              child: SingleChildScrollView(
                //controller: _scrollController,
                child: widget.child,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
