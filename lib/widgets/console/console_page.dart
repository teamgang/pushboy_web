import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/console/console_wrapper.dart';

class ConsolePage extends StatefulWidget {
  const ConsolePage({
    this.initialTab = 0,
  });
  final int initialTab;

  @override
  ConsolePageState createState() => ConsolePageState();
}

class ConsolePageState extends State<ConsolePage> {
  @override
  Widget build(BuildContext context) {
    King.of(context).dad.fetchProjects();

    return ConsoleWrapper(
      initialTab: widget.initialTab,
      child: Column(
        children: <Widget>[
          const SizedBox(height: 20),
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Text18('Your projects:'),
                ),
                IconButton(
                  icon: const Icon(Icons.refresh),
                  onPressed: () {
                    King.of(context).dad.fetchProjects();
                  },
                ),
                const Expanded(child: SizedBox.shrink()),
                TextButton(
                  child: const Text18('+ Create a new project'),
                  onPressed: () async {
                    await Navigator.of(context).pushNamed(Routes.projectCreate);
                    if (!mounted) return;
                    King.of(context).dad.fetchProjects();
                  },
                ),
              ]),
          const SizedBox(height: 20),
          Observer(builder: (_) {
            var projects = King.of(context).dad.projects.values.toList();

            return projects.isEmpty
                ? const Text('You have no projects')
                : ListView.separated(
                    shrinkWrap: true,
                    itemCount: projects.length,
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 20),
                    itemBuilder: (context, index) {
                      var project = projects[index];
                      return ProjectCard(project: project);
                    },
                  );
          }),
        ],
      ),
    );
  }
}

class ProjectCard extends StatelessWidget {
  const ProjectCard({required this.project});
  final Project project;

  @override
  Widget build(BuildContext context) {
    final plan = Plan();
    if (!plan.isLoaded) {
      plan.loadFromApi(King.of(context), project.planId);
    }

    return Card(
      elevation: 2,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(Routes.projectNetworksTab,
              arguments: Args(project: project));
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(project.name, style: const TextStyle(fontSize: 18)),
                const SizedBox(height: 4),
                const BaText('Notifications remaining:'),
                const SizedBox(height: 4),
                Observer(builder: (_) {
                  return Row(children: <Widget>[
                    BaText(
                        'Static: ${project.staticRemainingWithFree(plan).display}'),
                    const SizedBox(width: 12),
                    BaText(
                        'Custom: ${project.customRemainingWithFree(plan).display}'),
                    const SizedBox(width: 12),
                    BaText(
                        'Limitless: ${project.limitlessRemainingWithFree(plan).display}'),
                  ]);
                }),
              ]),
        ),
      ),
    );
  }
}
