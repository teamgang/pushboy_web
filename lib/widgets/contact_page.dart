import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ContactPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Column(
        children: const <Widget>[
          Text18('Questions or comments? Contact us via email:'),
          SizedBox(height: 4),
          SelectableText('team@pushboi.io', style: TextStyle(fontSize: 18)),
        ],
      ),
    );
  }
}
