import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ChannelChangeNotesPage extends StatefulWidget {
  const ChannelChangeNotesPage({required this.channel});
  final Channel channel;

  @override
  ChannelChangeNotesPageState createState() => ChannelChangeNotesPageState();
}

class ChannelChangeNotesPageState extends State<ChannelChangeNotesPage> {
  bool _canSave = false;
  String _currentValue = '';
  String _failureMsg = '';
  final int _maxLength = ValidateChannel.notesMaxLength;
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                initialValue: widget.channel.notes,
                keyboardType: TextInputType.multiline,
                maxLength: _maxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Notes',
                  counterText: '${_currentValue.length}/$_maxLength',
                ),
                onChanged: (newValue) {
                  _currentValue = newValue;
                  if (ValidateChannel.notes(_currentValue).isValid) {
                    setState(() {
                      _canSave = true;
                    });
                  } else {
                    setState(() {
                      _canSave = false;
                    });
                  }
                },
                validator: (value) {
                  return ValidateChannel.notes(_currentValue).asValidator;
                },
              ),
              const SizedBox(height: 12),
              Text('Edit your notes.', style: optionInfoStyle()),
              const SizedBox(height: 12),
              _requestInProgress
                  ? LoadingWidget()
                  : SizedBox(
                      height: 50,
                      width: 60,
                      child: ElevatedButton(
                        onPressed: _canSave
                            ? () {
                                FocusScope.of(context).unfocus();
                                _onSubmit(context);
                              }
                            : null,
                        child: const Text18('Save'),
                      ),
                    ),
              const SizedBox(height: 12),
              _failureMsg == ''
                  ? const SizedBox.shrink()
                  : Text(_failureMsg, style: failureStyle()),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      if (_currentValue == widget.channel.notes) {
        Navigator.of(context).pop();
      }

      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.channelChangeNotes,
        payload: {
          'channel_id': widget.channel.channelId,
          'notes': _currentValue,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (!mounted) return;
      if (ares.isOk) {
        widget.channel.loadFromApi(context, widget.channel.channelId);
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
