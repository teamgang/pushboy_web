import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ChannelChangeRoutePage extends StatefulWidget {
  const ChannelChangeRoutePage({required this.channel});
  final Channel channel;

  @override
  ChannelChangeRoutePageState createState() => ChannelChangeRoutePageState();
}

class ChannelChangeRoutePageState extends State<ChannelChangeRoutePage> {
  bool _canSave = false;
  String _currentValue = '';
  String _failureMsg = '';
  final int _maxLength = ValidateChannel.routeMaxLength;
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                initialValue: widget.channel.route,
                keyboardType: TextInputType.multiline,
                maxLength: _maxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Route',
                  counterText: '${_currentValue.length}/$_maxLength',
                ),
                onChanged: (newValue) {
                  _currentValue = newValue;
                  if (ValidateChannel.route(_currentValue).isValid) {
                    setState(() {
                      _canSave = true;
                    });
                  } else {
                    setState(() {
                      _canSave = false;
                    });
                  }
                },
                validator: (value) {
                  return ValidateChannel.route(_currentValue).asValidator;
                },
              ),
              const SizedBox(height: 12),
              Text('Edit your route.', style: optionInfoStyle()),
              const SizedBox(height: 12),
              _requestInProgress
                  ? LoadingWidget()
                  : SizedBox(
                      height: 50,
                      width: 60,
                      child: ElevatedButton(
                        onPressed: _canSave
                            ? () {
                                FocusScope.of(context).unfocus();
                                _onSubmit(context);
                              }
                            : null,
                        child: const Text18('Save'),
                      ),
                    ),
              const SizedBox(height: 12),
              FailureMsg(_failureMsg),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      if (_currentValue == widget.channel.route) {
        Navigator.of(context).pop();
      }

      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.channelChangeRoute,
        payload: {
          'channel_id': widget.channel.channelId,
          'route': _currentValue,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        if (!mounted) return;
        widget.channel.loadFromApi(context, widget.channel.channelId);
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
