import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/shared/crumbs.dart';
import 'package:pushboy_web/widgets/shared/divider_overview.dart';

class ChannelOverviewPage extends StatefulWidget {
  const ChannelOverviewPage({required this.channel});
  final Channel channel;

  @override
  ChannelOverviewPageState createState() => ChannelOverviewPageState();
}

class ChannelOverviewPageState extends State<ChannelOverviewPage> {
  @override
  Widget build(BuildContext context) {
    final channel = widget.channel;
    final network = widget.channel.getNetwork(context);
    final project = King.of(context).dad.projects[network.projectId];

    return WebScaffold(
      body: Scrollbar(
        thumbVisibility: true,
        child: SingleChildScrollView(
          child: Center(
            child: Observer(
              builder: (_) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Crumbs(
                    project: project,
                    network: network,
                    channel: widget.channel,
                  ),
                  const SizedBox(height: 24),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      FunctionCard(
                        text: 'Edit Channel',
                        onTap: () {
                          Navigator.of(context).pushNamed(Routes.channelEdit,
                              arguments: Args(channel: channel));
                        },
                      ),
                      const SizedBox(width: 14),
                      FunctionCard(
                        text: 'View Access Tokens',
                        onTap: () {
                          Navigator.of(context).pushNamed(
                              Routes.channelTokensOverview,
                              arguments: Args(channel: channel));
                        },
                      ),
                      const SizedBox(width: 14),
                      FunctionCard(
                        text: 'Metrics',
                        onTap: () {},
                      ),
                      const SizedBox(width: 14),
                      FunctionCard(
                        text: 'Edit Static Notification',
                        onTap: () {
                          Navigator.of(context).pushNamed(Routes.statEdit,
                              arguments: Args(channel: channel));
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 24),

                  const Text18('Channel Name'),
                  const SizedBox(height: 6),
                  Text(widget.channel.name,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  const Text18('Channel Abbreviation'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutChannelAbbrev, maxLines: 4),
                  const SizedBox(height: 6),
                  Text(widget.channel.abbrev,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  const Text18('Channel Subscription URL'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutChannelSubscriptionUrl,
                      maxLines: 4),
                  const SizedBox(height: 6),
                  Observer(builder: (_) {
                    final url =
                        'https://pushmeta.io/s/${network.route}/${widget.channel.route}';
                    return Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SelectableText(url,
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(width: 8),
                          IconButton(
                            icon: const Icon(Icons.copy, color: Colors.black),
                            onPressed: () {
                              Clipboard.setData(ClipboardData(text: url));
                              King.of(context)
                                  .snacker
                                  .addSnack(Snacks.copiedToClipboard);
                            },
                          ),
                        ]);
                  }),
                  const DividerOverview(),
                  //

                  const Text18('Description'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutChannelDescription),
                  const SizedBox(height: 6),
                  Text(widget.channel.description,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  const Text18('Notes'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutChannelNotes),
                  const SizedBox(height: 6),
                  Text(widget.channel.notes,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  const Text18('Channel preview for mobile users'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutChannelSubscriptionPreview),
                  const SizedBox(height: 12),
                  SizedBox(
                    width: 600,
                    child: ChannelSlab(
                      channel: channel,
                      onTap: () {},
                    ),
                  ),
                  const DividerOverview(),

                  const Text18('Static Notification Preview'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutStatPreview, maxLines: 8),
                  const SizedBox(height: 12),
                  const SizedBox(height: 20),
                  SizedBox(
                    width: 600,
                    child: StaticBoiSlab(channel: channel),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
