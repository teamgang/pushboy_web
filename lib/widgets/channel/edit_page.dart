import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/shared/crumbs.dart';
import 'package:pushboy_web/widgets/shared/divider_overview.dart';

class ChannelEditPage extends StatefulWidget {
  const ChannelEditPage({required this.channel});
  final Channel channel;

  @override
  ChannelEditPageState createState() => ChannelEditPageState();
}

class ChannelEditPageState extends State<ChannelEditPage> {
  @override
  Widget build(BuildContext context) {
    final channel = widget.channel;
    final network = widget.channel.getNetwork(context);
    final project = King.of(context).dad.projects[network.projectId];

    return WebScaffold(
      body: Scrollbar(
        thumbVisibility: true,
        child: SingleChildScrollView(
          child: Center(
            child: Observer(
              builder: (_) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Crumbs(
                    project: project,
                    network: network,
                    channel: channel,
                  ),
                  const SizedBox(height: 30),

                  Row(
                    children: <Widget>[
                      const Text18('Channel Name'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.channelChangeName,
                              arguments: Args(channel: channel));
                        },
                      ),
                    ],
                  ),
                  Text(channel.name,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  Row(
                    children: <Widget>[
                      const Text18('Abbreviation'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.channelChangeAbbrev,
                              arguments: Args(channel: channel));
                        },
                      ),
                    ],
                  ),
                  Text(channel.abbrev,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    const Text18('Channel Subscription URL'),
                    const SizedBox(width: 8),
                    TextButton(
                      child: const BaText('Edit'),
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                            Routes.channelChangeRoute,
                            arguments: Args(channel: channel));
                      },
                    ),
                  ]),
                  const SizedBox(height: 6),
                  Observer(builder: (_) {
                    final url =
                        'https://pushmeta.io/s/${network.route}/${channel.route}';
                    return Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SelectableText(url,
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(width: 8),
                          IconButton(
                            icon: const Icon(Icons.copy, color: Colors.black),
                            onPressed: () {
                              Clipboard.setData(ClipboardData(text: url));
                              King.of(context)
                                  .snacker
                                  .addSnack(Snacks.copiedToClipboard);
                            },
                          ),
                        ]);
                  }),
                  const DividerOverview(),
                  //

                  Row(
                    children: <Widget>[
                      const Text18('Description'),
                      const SizedBox(width: 8),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.channelChangeDescription,
                              arguments: Args(channel: channel));
                        },
                      ),
                    ],
                  ),
                  Text(channel.description,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  Row(
                    children: <Widget>[
                      const Text18('Notes'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.channelChangeNotes,
                              arguments: Args(channel: channel));
                        },
                      ),
                    ],
                  ),
                  Text(channel.notes,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  Row(children: <Widget>[
                    const Text18('Primary Color:'),
                    const SizedBox(width: 8),
                    ColorBlock(color: channel.colorPrimary),
                    const SizedBox(width: 8),
                    TextButton(
                      child: const BaText('Edit'),
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                            Routes.channelChangeColorPrimary,
                            arguments: Args(
                                channel: channel,
                                whichColor: WhichColor.primary));
                      },
                    ),
                  ]),
                  const SizedBox(height: 16),

                  Row(children: <Widget>[
                    const Text18('Secondary Color:'),
                    const SizedBox(width: 8),
                    ColorBlock(color: channel.colorSecondary),
                    const SizedBox(width: 8),
                    TextButton(
                      child: const BaText('Edit'),
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                            Routes.channelChangeColorSecondary,
                            arguments: Args(
                                channel: channel,
                                whichColor: WhichColor.secondary));
                      },
                    ),
                  ]),
                  const DividerOverview(),

                  Row(
                    children: <Widget>[
                      const Text18('Channel thumbnail'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.channelChangeThumb,
                              arguments: Args(channel: channel));
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 8),
                  ChannelThumbnails(channel),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ChannelThumbnails extends StatelessWidget {
  const ChannelThumbnails(this.channel);
  final Channel channel;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(
          children: <Widget>[
            BasedFadeInImage(
              altText: channel.abbrev,
              colorBackground: channel.colorPrimary,
              colorText: channel.colorSecondary,
              height: 80,
              width: 80,
              src: channel.thumbSelectedUrl,
            ),
            const SizedBox(height: 8),
            const Text16('Current thumbnail'),
          ],
        ),
        const SizedBox(width: 20),
        //

        Column(
          children: <Widget>[
            BasedFadeInImage(
              altText: channel.abbrev,
              colorBackground: channel.colorPrimary,
              colorText: channel.colorSecondary,
              height: 80,
              width: 80,
              src: '',
            ),
            const SizedBox(height: 8),
            const Text16('Thumbnail if there is an error'),
          ],
        ),
      ],
    );
  }
}
