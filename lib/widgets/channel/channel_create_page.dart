import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ChannelCreatePage extends StatefulWidget {
  const ChannelCreatePage({required this.network});
  final Network network;

  @override
  ChannelCreatePageState createState() => ChannelCreatePageState();
}

class ChannelCreatePageState extends State<ChannelCreatePage> {
  bool _canSave = false;
  String _failureMsg = '';
  late FocusNode _focusNode;
  bool _requestInProgress = false;
  String _abbrev = '';
  final Color _colorPrimary = getRandomPrimaryColor();
  final Color _colorSecondary = getRandomSecondaryColor();
  String _description = '';
  String _name = '';
  String _notes = '';
  String _route = '';
  String _staticBody = '';
  String _defaultThumbUrl = '';
  String _staticTitle = '';
  String _staticUrl = '';
  final GlobalKey _thumbnailKey = GlobalKey();

  @override
  initState() {
    super.initState();
    _focusNode = FocusNode();
    _focusNode.requestFocus();
    _defaultThumbUrl = King.of(context).conf.loadingImageUrl;
  }

  @override
  dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _onAnyChange() {
    if (ValidateChannel.allFields(
      abbrev: _abbrev,
      description: _description,
      name: _name,
      notes: _notes,
      route: _route,
      staticBody: _staticBody,
      staticThumbUrl: _defaultThumbUrl,
      staticTitle: _staticTitle,
      staticUrl: _staticUrl,
      thumbUrl: _defaultThumbUrl,
    )) {
      setState(() {
        _canSave = true;
      });
    } else {
      setState(() {
        _canSave = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const SizedBox(height: 30),
                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  enabled: !_requestInProgress,
                  focusNode: _focusNode,
                  keyboardType: TextInputType.multiline,
                  maxLength: ValidateChannel.nameMaxLength,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Name *',
                    counterText:
                        '${_name.length}/${ValidateChannel.nameMaxLength}',
                  ),
                  onChanged: (newValue) {
                    _name = newValue;
                    if (_abbrev.isEmpty) {
                      _abbrev = newValue;
                    }
                    _onAnyChange();
                  },
                  validator: (value) {
                    return ValidateChannel.name(_name).asValidator;
                  },
                ),
                //

                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  enabled: !_requestInProgress,
                  keyboardType: TextInputType.multiline,
                  maxLength: ValidateChannel.abbrevMaxLength,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Channel Abbreviation',
                    counterText:
                        '${_abbrev.length}/${ValidateChannel.abbrevMaxLength}',
                  ),
                  onChanged: (newValue) {
                    _abbrev = newValue;
                    _onAnyChange();
                  },
                  validator: (value) {
                    return ValidateChannel.abbrev(_abbrev).asValidator;
                  },
                ),

                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  enabled: !_requestInProgress,
                  keyboardType: TextInputType.multiline,
                  maxLength: ValidateChannel.descriptionMaxLength,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Description (for your audience)',
                    counterText:
                        '${_description.length}/${ValidateChannel.descriptionMaxLength}',
                  ),
                  onChanged: (newValue) {
                    _description = newValue;
                    _onAnyChange();
                  },
                  validator: (value) {
                    return ValidateChannel.description(_description)
                        .asValidator;
                  },
                ),

                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  enabled: !_requestInProgress,
                  keyboardType: TextInputType.multiline,
                  maxLength: ValidateChannel.routeMaxLength,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'URL route endpoint *',
                    counterText:
                        '${_route.length}/${ValidateChannel.routeMaxLength}',
                  ),
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9!.]')),
                  ],
                  onChanged: (newValue) {
                    setState(() {
                      _route = newValue;
                    });
                    _onAnyChange();
                  },
                  validator: (value) {
                    return ValidateChannel.route(_route).asValidator;
                  },
                ),
                const Text(
                    'Your followers can subscribe to this channel (and it\'s network) with the following URL:'),
                Text('https://pushboi.io/d/${widget.network.route}/$_route'),
                const SizedBox(height: 12),
                //

                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  enabled: !_requestInProgress,
                  keyboardType: TextInputType.multiline,
                  maxLength: ValidateChannel.staticTitleMaxLength,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Static notification message title',
                    counterText:
                        '${_staticTitle.length}/${ValidateChannel.staticTitleMaxLength}',
                  ),
                  onChanged: (newValue) {
                    _staticTitle = newValue;
                    _onAnyChange();
                  },
                  validator: (value) {
                    return ValidateChannel.staticTitle(_staticTitle)
                        .asValidator;
                  },
                ),
                //

                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  enabled: !_requestInProgress,
                  keyboardType: TextInputType.multiline,
                  maxLength: ValidateChannel.staticBodyMaxLength,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Static notification message',
                    counterText:
                        '${_staticBody.length}/${ValidateChannel.staticBodyMaxLength}',
                  ),
                  onChanged: (newValue) {
                    _staticBody = newValue;
                    _onAnyChange();
                  },
                  validator: (value) {
                    return ValidateChannel.staticBody(_staticBody).asValidator;
                  },
                ),
                //

                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  enabled: !_requestInProgress,
                  keyboardType: TextInputType.multiline,
                  maxLength: ValidateChannel.staticUrlMaxLength,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Static Notification opens this URL',
                    counterText:
                        '${_staticUrl.length}/${ValidateChannel.staticUrlMaxLength}',
                  ),
                  onChanged: (newValue) {
                    _staticUrl = newValue;
                    _onAnyChange();
                  },
                  validator: (value) {
                    return ValidateChannel.staticUrl(_staticUrl).asValidator;
                  },
                ),

                TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  enabled: !_requestInProgress,
                  keyboardType: TextInputType.multiline,
                  maxLength: ValidateChannel.notesMaxLength,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText:
                        'Notes (for internal use; your users will not see this)',
                    counterText:
                        '${_notes.length}/${ValidateChannel.notesMaxLength}',
                  ),
                  onChanged: (newValue) {
                    _notes = newValue;
                    _onAnyChange();
                  },
                  validator: (value) {
                    return ValidateChannel.notes(_notes).asValidator;
                  },
                ),
                const SizedBox(height: 12),
                //

                const Text18(
                    'Channel icon if image loading fails. You can change this later.'),
                const SizedBox(height: 12),
                Row(children: <Widget>[
                  DefaultChannelThumbnail(
                    abbrev: _abbrev,
                    colorBackground: _colorPrimary,
                    colorText: _colorSecondary,
                    saveKey: _thumbnailKey,
                    height: 300,
                    width: 300,
                    fontSize: 76,
                  ),
                  const SizedBox.shrink(),
                ]),
                const SizedBox(height: 12),

                FailureMsg(_failureMsg),
                const SizedBox(height: 20),

                Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      TextButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          child: const Text('Cancel')),
                      TextButton(
                          onPressed: _canSave ? () => _onSubmit(context) : null,
                          child: const Text('Create')),
                    ]),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    var king = King.of(context);
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      String thumbB64 =
          Uri.encodeFull(base64Encode(await widgetToImage(_thumbnailKey)));

      ApiResponse ares = await king.lip.api(
        EndpointsV1.channelCreate,
        payload: {
          'abbrev': _abbrev,
          'color_primary': stringFromColor(_colorPrimary),
          'color_secondary': stringFromColor(_colorSecondary),
          'description': _description,
          'name': _name,
          'network_id': widget.network.networkId,
          'notes': _notes,
          'route': _route,
          'static_body': _staticBody,
          'static_title': _staticTitle,
          'static_url': _staticUrl,
          'thumb_b64': thumbB64,
        },
      );

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        if (!mounted) return;
        Navigator.of(context).pop();
      } else {
        setState(() {
          _failureMsg =
              ares.peekErrorMsg(defaultText: 'Error creating channel.');
        });
      }
    }
  }
}
