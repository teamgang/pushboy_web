import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ChannelChangeDescriptionPage extends StatefulWidget {
  const ChannelChangeDescriptionPage({required this.channel});
  final Channel channel;

  @override
  ChannelChangeDescriptionPageState createState() =>
      ChannelChangeDescriptionPageState();
}

class ChannelChangeDescriptionPageState
    extends State<ChannelChangeDescriptionPage> {
  bool _canSave = false;
  String _currentValue = '';
  String _failureMsg = '';
  final int _maxLength = ValidateChannel.descriptionMaxLength;
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                initialValue: widget.channel.description,
                keyboardType: TextInputType.multiline,
                maxLength: _maxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Description',
                  counterText: '${_currentValue.length}/$_maxLength',
                ),
                onChanged: (newValue) {
                  _currentValue = newValue;
                  if (ValidateChannel.description(_currentValue).isValid) {
                    setState(() {
                      _canSave = true;
                    });
                  } else {
                    setState(() {
                      _canSave = false;
                    });
                  }
                },
                validator: (value) {
                  return ValidateChannel.description(_currentValue).asValidator;
                },
              ),
              const SizedBox(height: 12),
              Text('Edit the description.', style: optionInfoStyle()),
              const SizedBox(height: 12),
              _requestInProgress
                  ? LoadingWidget()
                  : SizedBox(
                      height: 50,
                      width: 60,
                      child: ElevatedButton(
                        onPressed: _canSave
                            ? () {
                                FocusScope.of(context).unfocus();
                                _onSubmit(context);
                              }
                            : null,
                        child: const Text18('Save'),
                      ),
                    ),
              const SizedBox(height: 12),
              FailureMsg(_failureMsg),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      if (_currentValue == widget.channel.description) {
        Navigator.of(context).pop();
      }

      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.channelChangeDescription,
        payload: {
          'channel_id': widget.channel.channelId,
          'description': _currentValue,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        if (!mounted) return;
        widget.channel.loadFromApi(context, widget.channel.channelId);
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
