import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class NetworkCreatePage extends StatefulWidget {
  const NetworkCreatePage({required this.project});
  final Project project;

  @override
  NetworkCreatePageState createState() => NetworkCreatePageState();
}

class NetworkCreatePageState extends State<NetworkCreatePage> {
  bool _canSave = false;
  String _failureMsg = '';
  late FocusNode _focusNode;
  bool _requestInProgress = false;
  String _abbrev = '';
  final Color _colorPrimary = getRandomPrimaryColor();
  final Color _colorSecondary = getRandomSecondaryColor();
  String _description = '';
  String _name = '';
  String _notes = '';
  String _route = '';
  final GlobalKey _thumbnailKey = GlobalKey();

  @override
  initState() {
    super.initState();
    _focusNode = FocusNode();
    _focusNode.requestFocus();
  }

  @override
  dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _onAnyChange() {
    if (ValidateNetwork.allFields(
      abbrev: _abbrev,
      description: _description,
      name: _name,
      notes: _notes,
      route: _route,
    )) {
      setState(() {
        _canSave = true;
      });
    } else {
      setState(() {
        _canSave = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                focusNode: _focusNode,
                keyboardType: TextInputType.multiline,
                maxLength: ValidateNetwork.nameMaxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Name',
                  counterText:
                      '${_name.length}/${ValidateNetwork.nameMaxLength}',
                ),
                onChanged: (newValue) {
                  _name = newValue;
                  if (_abbrev.isEmpty) {
                    _abbrev = newValue;
                  }
                  _onAnyChange();
                },
                validator: (value) {
                  return ValidateNetwork.name(_name).asValidator;
                },
              ),
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                keyboardType: TextInputType.multiline,
                maxLength: ValidateNetwork.abbrevMaxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Channel Abbreviation',
                  counterText:
                      '${_abbrev.length}/${ValidateNetwork.abbrevMaxLength}',
                ),
                onChanged: (newValue) {
                  _abbrev = newValue;
                  _onAnyChange();
                },
                validator: (value) {
                  return ValidateNetwork.abbrev(_abbrev).asValidator;
                },
              ),

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                keyboardType: TextInputType.multiline,
                maxLength: ValidateNetwork.descriptionMaxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Description (shown to your audience)',
                  counterText:
                      '${_description.length}/${ValidateNetwork.descriptionMaxLength}',
                ),
                onChanged: (newValue) {
                  _description = newValue;
                  _onAnyChange();
                },
                validator: (value) {
                  return ValidateNetwork.description(_description).asValidator;
                },
              ),
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                keyboardType: TextInputType.multiline,
                maxLength: ValidateNetwork.routeMaxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'URL Route (for customer subscriptions)',
                  counterText:
                      '${_route.length}/${ValidateNetwork.routeMaxLength}',
                ),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp('[a-zA-Z0-9!.]')),
                ],
                onChanged: (newValue) {
                  setState(() {
                    _route = newValue.replaceAll(RegExp(r'\s+'), '');
                  });
                  _onAnyChange();
                },
                validator: (value) {
                  return ValidateNetwork.route(_route).asValidator;
                },
              ),
              const Text(
                  'Your followers will use URLs with the following format to subscribe to this network and it\'s channels.'),
              Text(
                  'https://pushboi.io/d/$_route/\${optional_channel_name_goes_here}'),
              //

              const SizedBox(height: 20),
              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                keyboardType: TextInputType.multiline,
                maxLength: ValidateNetwork.notesMaxLength,
                minLines: 3,
                maxLines: 8,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Notes (for you; users will not see this)',
                  counterText:
                      '${_notes.length}/${ValidateNetwork.notesMaxLength}',
                ),
                onChanged: (newValue) {
                  _notes = newValue;
                  _onAnyChange();
                },
                validator: (value) {
                  return ValidateNetwork.notes(_notes).asValidator;
                },
              ),
              //
              const Text18(
                  'Channel icon if image loading fails. You can change this later.'),
              const SizedBox(height: 12),
              Row(children: <Widget>[
                DefaultChannelThumbnail(
                  abbrev: _abbrev,
                  colorBackground: _colorPrimary,
                  colorText: _colorSecondary,
                  saveKey: _thumbnailKey,
                  height: 300,
                  width: 300,
                  fontSize: 76,
                ),
                const SizedBox.shrink(),
              ]),
              const SizedBox(height: 12),

              const SizedBox(height: 12),
              FailureMsg(_failureMsg),
              const SizedBox(height: 12),
              Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    TextButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        child: const Text18('Cancel')),
                    TextButton(
                        onPressed: _canSave ? () => _onSubmit(context) : null,
                        child: const Text18('Create')),
                  ]),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    var king = King.of(context);
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      String thumbB64 =
          Uri.encodeFull(base64Encode(await widgetToImage(_thumbnailKey)));

      var payload = {
        'abbrev': _abbrev,
        'color_primary': stringFromColor(_colorPrimary),
        'color_secondary': stringFromColor(_colorSecondary),
        'description': _description,
        'name': _name,
        'notes': _notes,
        'project_id': widget.project.projectId,
        'route': _route,
        'thumb_b64': thumbB64,
      };

      ApiResponse ares = await king.lip.api(
        EndpointsV1.networkCreate,
        payload: payload,
      );

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        if (!mounted) return;
        Navigator.of(context).pop();
      } else {
        setState(() {
          _failureMsg =
              ares.peekErrorMsg(defaultText: 'Error creating network.');
        });
      }
    }
  }
}
