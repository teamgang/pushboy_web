import 'dart:convert';
import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class NetworkChangeAbbrevPage extends StatefulWidget {
  const NetworkChangeAbbrevPage({required this.network});
  final Network network;

  @override
  NetworkChangeAbbrevPageState createState() => NetworkChangeAbbrevPageState();
}

class NetworkChangeAbbrevPageState extends State<NetworkChangeAbbrevPage> {
  bool _canSave = false;
  String _currentValue = '';
  String _failureMsg = '';
  final int _maxLength = ValidateNetwork.abbrevMaxLength;
  bool _requestInProgress = false;
  final GlobalKey _thumbnailKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    final network = widget.network;

    return WebScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                initialValue: widget.network.abbrev,
                keyboardType: TextInputType.multiline,
                maxLength: _maxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Abbreviation',
                  counterText: '${_currentValue.length}/$_maxLength',
                ),
                onChanged: (newValue) {
                  _currentValue = newValue;
                  if (ValidateNetwork.abbrev(_currentValue).isValid) {
                    setState(() {
                      _canSave = true;
                    });
                  } else {
                    setState(() {
                      _canSave = false;
                    });
                  }
                },
                validator: (value) {
                  return ValidateNetwork.abbrev(_currentValue).asValidator;
                },
              ),
              const SizedBox(height: 12),
              Text('Change the abbreviation of this network.',
                  style: optionInfoStyle()),
              const SizedBox(height: 12),

              const Text18('Updated network icon if image loading fails.'),
              const SizedBox(height: 12),
              Row(children: <Widget>[
                DefaultChannelThumbnail(
                  abbrev: _currentValue,
                  colorBackground: network.colorPrimary,
                  colorText: network.colorSecondary,
                  saveKey: _thumbnailKey,
                  height: 300,
                  width: 300,
                  fontSize: 76,
                ),
                const SizedBox.shrink(),
              ]),
              const SizedBox(height: 12),

              _requestInProgress
                  ? LoadingWidget()
                  : SizedBox(
                      height: 50,
                      width: 60,
                      child: ElevatedButton(
                        onPressed: _canSave
                            ? () {
                                FocusScope.of(context).unfocus();
                                _onSubmit(context);
                              }
                            : null,
                        child: const Text18('Save'),
                      ),
                    ),
              const SizedBox(height: 12),
              FailureMsg(_failureMsg),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    final king = King.of(context);
    if (!_requestInProgress) {
      if (_currentValue == widget.network.abbrev) {
        Navigator.of(context).pop();
      }

      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      String thumbB64 =
          Uri.encodeFull(base64Encode(await widgetToImage(_thumbnailKey)));

      ApiResponse ares = await king.lip.api(
        EndpointsV1.networkChangeAbbrev,
        payload: {
          'network_id': widget.network.networkId,
          'abbrev': _currentValue,
          'thumb_b64': thumbB64,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        imageCache.clear();
        if (!mounted) return;
        widget.network.loadFromApi(context, widget.network.networkId);
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
