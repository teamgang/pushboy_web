import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flex_color_picker/flex_color_picker.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

Color errorColor = const Color(0xFF00CC88);

class NetworkChangeColorPage extends StatefulWidget {
  const NetworkChangeColorPage({
    required this.network,
    required this.whichColor,
  });
  final Network network;
  final WhichColor whichColor;

  @override
  NetworkChangeColorPageState createState() => NetworkChangeColorPageState();
}

class NetworkChangeColorPageState extends State<NetworkChangeColorPage> {
  bool _canSave = false;
  Color _currentValue = errorColor;
  Color _initialValue = errorColor;
  final TextEditingController _rCon = TextEditingController(text: '');
  final TextEditingController _bCon = TextEditingController(text: '');
  final TextEditingController _gCon = TextEditingController(text: '');
  String _failureMsg = '';
  bool _requestInProgress = false;
  String _endpoint = '';
  String _title = '';
  final GlobalKey _thumbnailKey = GlobalKey();

  @override
  initState() {
    super.initState();
    switch (widget.whichColor) {
      case WhichColor.primary:
        _title = 'Set network primary color';
        _endpoint = EndpointsV1.networkChangeColorPrimary;
        _initialValue = widget.network.colorPrimary;
        break;

      case WhichColor.secondary:
        _title = 'Set network secondary color';
        _endpoint = EndpointsV1.networkChangeColorSecondary;
        _initialValue = widget.network.colorSecondary;
        break;

      default:
        throw Exception('Invalid color option');
    }

    _rCon.text = _initialValue.red.toString();
    _gCon.text = _initialValue.green.toString();
    _bCon.text = _initialValue.blue.toString();
    _currentValue = _initialValue;
  }

  void _setCurrentColor() {
    if (!_canSave) {
      _canSave = true;
    }
    setState(() {
      _currentValue = Color.fromRGBO(
        int.parse(_rCon.text),
        int.parse(_gCon.text),
        int.parse(_bCon.text),
        1.0,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
    final network = widget.network;

    return WebScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //
              SizedBox(
                width: double.infinity,
                child: Padding(
                  padding: const EdgeInsets.all(6),
                  child: Card(
                    elevation: 2,
                    child: ColorPicker(
                      color: _currentValue,
                      onColorChanged: (Color color) {
                        _rCon.text = color.red.toString();
                        _gCon.text = color.green.toString();
                        _bCon.text = color.blue.toString();
                        _setCurrentColor();
                      },
                      width: 44,
                      height: 44,
                      borderRadius: 22,
                      heading: Text(
                        _title,
                        style: Theme.of(context).textTheme.headline5,
                      ),
                      subheading: Text(
                        'Shades',
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                      pickersEnabled: const <ColorPickerType, bool>{
                        ColorPickerType.accent: false,
                        ColorPickerType.primary: true,
                        ColorPickerType.custom: true,
                      },
                      showColorCode: true,
                    ),
                  ),
                ),
              ),

              const SizedBox(height: 30),
              Text('Selected color:', style: optionInfoStyle()),
              ColorBlock(color: _currentValue, height: 50),

              const SizedBox(height: 60),

              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    width: 100,
                    child: TextFormField(
                      controller: _rCon,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                      enabled: !_requestInProgress,
                      maxLength: 3,
                      style: Theme.of(context).textTheme.headline6,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Red:',
                        hintText: 'RGB red value',
                      ),
                      onChanged: (String newValue) {
                        int asNum = int.parse(newValue);
                        if (asNum > 255) {
                          _rCon.text = '255';
                        } else if (asNum < 0) {
                          _rCon.text = '0';
                        }
                        _setCurrentColor();
                      },
                    ),
                  ),
                  const SizedBox(width: 20),
                  //

                  SizedBox(
                    width: 100,
                    child: TextFormField(
                      controller: _gCon,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                      enabled: !_requestInProgress,
                      maxLength: 3,
                      style: Theme.of(context).textTheme.headline6,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Green:',
                        hintText: 'RGB green value',
                      ),
                      onChanged: (String newValue) {
                        int asNum = int.parse(newValue);
                        if (asNum > 255) {
                          _gCon.text = '255';
                        } else if (asNum < 0) {
                          _gCon.text = '0';
                        }
                        _setCurrentColor();
                      },
                    ),
                  ),
                  const SizedBox(width: 20),

                  SizedBox(
                    width: 100,
                    child: TextFormField(
                      controller: _bCon,
                      keyboardType: TextInputType.number,
                      inputFormatters: [
                        FilteringTextInputFormatter.digitsOnly,
                      ],
                      enabled: !_requestInProgress,
                      maxLength: 3,
                      style: Theme.of(context).textTheme.headline6,
                      decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Blue:',
                        hintText: 'RGB blue value',
                      ),
                      onChanged: (String newValue) {
                        int asNum = int.parse(newValue);
                        if (asNum > 255) {
                          _bCon.text = '255';
                        } else if (asNum < 0) {
                          _bCon.text = '0';
                        }
                        _setCurrentColor();
                      },
                    ),
                  ),
                ],
              ),

              const Text18('Updated channel icon if image loading fails.'),
              const SizedBox(height: 12),
              Row(children: <Widget>[
                DefaultChannelThumbnail(
                  abbrev: network.abbrev,
                  saveKey: _thumbnailKey,
                  height: 300,
                  width: 300,
                  fontSize: 76,
                  colorBackground: widget.whichColor == WhichColor.primary
                      ? _currentValue
                      : network.colorPrimary,
                  colorText: widget.whichColor == WhichColor.secondary
                      ? _currentValue
                      : network.colorSecondary,
                ),
                const SizedBox.shrink(),
              ]),
              const SizedBox(height: 12),

              const SizedBox(height: 40),
              _requestInProgress
                  ? LoadingWidget()
                  : SizedBox(
                      height: 50,
                      width: 60,
                      child: ElevatedButton(
                        onPressed: _canSave
                            ? () {
                                FocusScope.of(context).unfocus();
                                _onSubmit(context);
                              }
                            : null,
                        child: const Text18('Save'),
                      ),
                    ),
              const SizedBox(height: 12),
              FailureMsg(_failureMsg),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    final king = King.of(context);
    if (!_requestInProgress) {
      if (_currentValue == _initialValue) {
        Navigator.of(context).pop();
      }

      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      String thumbB64 =
          Uri.encodeFull(base64Encode(await widgetToImage(_thumbnailKey)));

      ApiResponse ares = await king.lip.api(
        _endpoint,
        payload: {
          'network_id': widget.network.networkId,
          'color': stringFromColor(_currentValue),
          'thumb_b64': thumbB64,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        imageCache.clear();
        if (!mounted) return;
        widget.network.loadFromApi(context, widget.network.networkId);
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
