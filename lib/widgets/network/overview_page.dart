import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/shared/crumbs.dart';
import 'package:pushboy_web/widgets/shared/divider_overview.dart';

class NetworkOverviewPage extends StatefulWidget {
  const NetworkOverviewPage({required this.network});
  final Network network;

  @override
  NetworkOverviewPageState createState() => NetworkOverviewPageState();
}

class NetworkOverviewPageState extends State<NetworkOverviewPage> {
  @override
  initState() {
    super.initState();
    widget.network.loadFromApi(context, widget.network.networkId);
  }

  Network get _network {
    return widget.network;
  }

  @override
  Widget build(BuildContext context) {
    final project = King.of(context).dad.projects[_network.projectId];

    final channels =
        King.of(context).dad.channels.getChannelsOfNetwork(_network.networkId);

    return WebScaffold(
      body: Scrollbar(
        thumbVisibility: true,
        child: SingleChildScrollView(
          child: Center(
            child: Observer(
              builder: (_) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Crumbs(project: project, network: _network),
                  const SizedBox(height: 24),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      FunctionCard(
                        text: 'Edit Network',
                        onTap: () {
                          Navigator.of(context).pushNamed(Routes.networkEdit,
                              arguments: Args(network: _network));
                        },
                      ),
                      const SizedBox(width: 14),
                      FunctionCard(
                        text: 'View Access Tokens',
                        onTap: () {
                          Navigator.of(context).pushNamed(
                              Routes.networkTokensOverview,
                              arguments: Args(network: _network));
                        },
                      ),
                      const SizedBox(width: 14),
                      FunctionCard(
                        text: 'Metrics',
                        onTap: () {
                          Navigator.of(context).pushNamed(Routes.networkMetrics,
                              arguments: Args(network: _network));
                        },
                      ),
                      const SizedBox(width: 14),
                      FunctionCard(
                        text: 'Send Notification',
                        onTap: () {
                          Navigator.of(context).pushNamed(Routes.boiSend,
                              arguments: Args(network: _network));
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 24),

                  const Text18('Network Name'),
                  const SizedBox(height: 6),
                  Text(_network.name,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  const Text18('Network Abbreviation'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutNetworkAbbrev, maxLines: 4),
                  const SizedBox(height: 6),
                  Text(_network.abbrev,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  const Text18('Network Subscription URL'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutNetworkSubscriptionUrl,
                      maxLines: 4),
                  const SizedBox(height: 6),
                  Observer(builder: (_) {
                    final url = 'https://pushmeta.io/s/${widget.network.route}';
                    return Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SelectableText(url,
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(width: 8),
                          IconButton(
                            icon: const Icon(Icons.copy, color: Colors.black),
                            onPressed: () {
                              Clipboard.setData(ClipboardData(text: url));
                              King.of(context)
                                  .snacker
                                  .addSnack(Snacks.copiedToClipboard);
                            },
                          ),
                        ]);
                  }),
                  const DividerOverview(),
                  //

                  const Text18('Description'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutNetworkDescription),
                  const SizedBox(height: 6),
                  Text(widget.network.description,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  const Text18('Notes'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutNetworkNotes),
                  const SizedBox(height: 6),
                  Text(widget.network.notes,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  const Text18('Subscription preview for mobile users'),
                  const SizedBox(height: 6),
                  const Text16(Description.aboutNetworkSubscriptionPreview),
                  const SizedBox(height: 12),
                  SizedBox(
                    width: 600,
                    child: NetworkSlab(
                      network: widget.network,
                      onTap: () {},
                    ),
                  ),

                  const DividerOverview(),

                  Observer(
                    builder: (_) {
                      return Text18('Channels (${channels.length}):');
                    },
                  ),
                  const SizedBox(height: 20),
                  FunctionCard(
                    text: '+ Add a Channel',
                    onTap: () async {
                      await Navigator.of(context).pushNamed(
                          Routes.channelCreate,
                          arguments: Args(network: widget.network));
                      if (!mounted) return;
                      King.of(context)
                          .dad
                          .channels
                          .getChannelsOfNetworkFromApi(_network.networkId);
                    },
                  ),
                  const SizedBox(height: 20),
                  Observer(builder: (_) {
                    return ListView.separated(
                      shrinkWrap: true,
                      itemCount: channels.length,
                      separatorBuilder: (context, index) =>
                          const SizedBox(height: 12),
                      itemBuilder: (context, index) {
                        return ChannelCard(
                          channel: channels[index],
                          index: index,
                          network: widget.network,
                        );
                      },
                    );
                  })
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class ChannelCard extends StatefulWidget {
  const ChannelCard({
    required this.channel,
    required this.index,
    required this.network,
  });
  final int index;
  final Channel channel;
  final Network network;

  @override
  ChannelCardState createState() => ChannelCardState();
}

class ChannelCardState extends State<ChannelCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(Routes.channelOverview,
              arguments:
                  Args(channel: widget.channel, network: widget.network));
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text18('${widget.index + 1}. ${widget.channel.name}'),
                const SizedBox(height: 4),
                BaText(widget.channel.description),
                const SizedBox(height: 4),
                BaText(widget.channel.notes),
                const SizedBox(height: 4),
                BaText('Route: ${widget.channel.route}'),
              ]),
        ),
      ),
    );
  }
}
