import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class NetworkChangeRoutePage extends StatefulWidget {
  const NetworkChangeRoutePage({required this.network});
  final Network network;

  @override
  _NetworkChangeRoutePageState createState() => _NetworkChangeRoutePageState();
}

class _NetworkChangeRoutePageState extends State<NetworkChangeRoutePage> {
  bool _canSave = false;
  String _currentValue = '';
  String _failureMsg = '';
  final int _maxLength = ValidateNetwork.routeMaxLength;
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                initialValue: widget.network.route,
                keyboardType: TextInputType.multiline,
                maxLength: _maxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Route',
                  counterText: '${_currentValue.length}/$_maxLength',
                ),
                onChanged: (newValue) {
                  _currentValue = newValue;
                  if (ValidateNetwork.route(_currentValue).isValid) {
                    setState(() {
                      _canSave = true;
                    });
                  } else {
                    setState(() {
                      _canSave = false;
                    });
                  }
                },
                validator: (value) {
                  return ValidateNetwork.route(_currentValue).asValidator;
                },
              ),
              const SizedBox(height: 12),
              Text('Edit your route.', style: optionInfoStyle()),
              const SizedBox(height: 12),
              _requestInProgress
                  ? LoadingWidget()
                  : SizedBox(
                      height: 50,
                      width: 60,
                      child: ElevatedButton(
                        child: const Text18('Save'),
                        onPressed: _canSave
                            ? () {
                                FocusScope.of(context).unfocus();
                                _onSubmit(context);
                              }
                            : null,
                      ),
                    ),
              const SizedBox(height: 12),
              _failureMsg == ''
                  ? const SizedBox.shrink()
                  : Text(_failureMsg, style: failureStyle()),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      if (_currentValue == widget.network.route) {
        Navigator.of(context).pop();
      }

      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.networkChangeRoute,
        payload: {
          'network_id': widget.network.networkId,
          'route': _currentValue,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        widget.network.loadFromApi(context, widget.network.networkId);
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
