import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class NetworkTokensOverviewPage extends StatefulWidget {
  const NetworkTokensOverviewPage({required this.network});
  final Network network;

  @override
  _NetworkTokensOverviewPageState createState() =>
      _NetworkTokensOverviewPageState();
}

class _NetworkTokensOverviewPageState extends State<NetworkTokensOverviewPage> {
  bool _requestInProgress = false;
  String _failureMsg = '';

  @override
  Widget build(BuildContext context) {
    final network = widget.network;
    network.loadTokensFromApi(context);

    return WebScaffold(
      body: Scrollbar(
        isAlwaysShown: true,
        child: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(height: 30),
                const Text('Access tokens for this network:'),
                const SizedBox(height: 20),
                Card(
                  elevation: 2,
                  child: InkWell(
                    onTap: () {
                      _createToken(context);
                      widget.network.loadTokensFromApi(context);
                    },
                    child: const Padding(
                      padding:
                          EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                      child: Text('Create a new token'),
                    ),
                  ),
                ),
                const SizedBox(height: 10),
                _failureMsg == ''
                    ? const SizedBox.shrink()
                    : Text(_failureMsg, style: failureStyle()),
                const SizedBox(height: 10),
                Observer(builder: (_) {
                  var tokens = network.tokens;

                  return ListView.separated(
                    shrinkWrap: true,
                    itemCount: tokens.length,
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 12),
                    itemBuilder: (context, index) {
                      return TokenCard(
                        network: widget.network,
                        token: tokens[index],
                      );
                    },
                  );
                })
              ],
            ),
          ),
        ),
      ),
    );
  }

  _createToken(BuildContext context) async {
    var king = King.of(context);
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await king.lip.api(
        EndpointsV1.networkTokenCreate,
        payload: {'network_id': widget.network.networkId},
      );

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
      } else {
        setState(() {
          _failureMsg = 'Failed to create network token.';
        });
      }
    }
  }
}

class TokenCard extends StatefulWidget {
  const TokenCard({required this.network, required this.token});
  final Network network;
  final NetworkToken token;

  @override
  _TokenCardState createState() => _TokenCardState();
}

class _TokenCardState extends State<TokenCard> {
  String _failureMsg = '';
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
        onTap: () {},
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(children: <Widget>[
            Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <
                Widget>[
              SelectableText(widget.token.token),
              const Expanded(child: SizedBox.shrink()),
              IconButton(
                icon: const Icon(Icons.copy, color: Colors.black),
                onPressed: () {
                  Clipboard.setData(ClipboardData(text: widget.token.token));
                  King.of(context).snacker.addSnack(Snacks.copiedToClipboard);
                },
              ),
              IconButton(
                icon: const Icon(Icons.delete, color: Colors.red),
                onPressed: () async {
                  await showDialog(
                    context: context,
                    builder: (context) => AlertDialog(
                        title: const Text('Delete token?'),
                        content: const Text(
                            'Are you sure you want to delete this token?'),
                        actions: <Widget>[
                          TextButton(
                            onPressed: () => Navigator.of(context).pop(false),
                            child: const Text('No'),
                          ),
                          TextButton(
                            onPressed: () {
                              _deleteToken(context);
                              Navigator.of(context).pop(false);
                            },
                            child: const Text('Yes'),
                          ),
                        ]),
                  );

                  widget.network.loadTokensFromApi(context);
                },
              ),
            ]),
            _failureMsg == ''
                ? const SizedBox.shrink()
                : Text(_failureMsg, style: failureStyle()),
          ]),
        ),
      ),
    );
  }

  _deleteToken(BuildContext context) async {
    var king = King.of(context);
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await king.lip.api(
        EndpointsV1.networkTokenDelete,
        payload: {'token': widget.token.token},
      );

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        widget.network.loadTokensFromApi(context);
      } else {
        setState(() {
          _failureMsg = 'Failed to delete network token.';
        });
      }
    }
  }
}
