import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/utils/select_period_data.dart';
import 'package:pushboy_web/widgets/shared/select_period.dart';

class NetworkMetricsPage extends StatefulWidget {
  const NetworkMetricsPage({required this.network});
  final Network network;

  @override
  _NetworkMetricsPageState createState() => _NetworkMetricsPageState();
}

class _NetworkMetricsPageState extends State<NetworkMetricsPage> {
  final SelectPeriodData data = SelectPeriodData();

  @override
  initState() {
    super.initState();
    data.selectedMonth = data.currentMonth;
    data.selectedYear = data.currentYear;
  }

  @override
  Widget build(BuildContext context) {
    final bois = King.of(context).dad.bois.getBoisOfNetworkAndPeriod(
          networkId: widget.network.networkId,
          year: data.selectedYear,
          month: data.selectedMonth,
        );
    final calc = King.of(context).dad.bois.latestCalc;

    return WebScaffold(
      body: Scrollbar(
        isAlwaysShown: true,
        child: Center(
          child: Observer(
            builder: (_) => Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(height: 24),
                SelectPeriod(
                    data: data,
                    onChange: () {
                      setState(() {});
                    }),
                const SizedBox(height: 24),
                //

                data.isSelectedDateValid
                    ? Text18(
                        'Showing notifications for the period of ${data.printSelectedDate}.')
                    : const Text18(
                        'Select a period to view the notifications.'),

                const SizedBox(height: 30),
                SimpleRow(
                    left: 'Total notifications received',
                    right: calc.cmCreated.display),
                const SizedBox(height: 6),
                SimpleRow(
                    left: 'Total notifications viewed (impressions)',
                    right: calc.cmViewedInApp.display),
                const SizedBox(height: 30),

                Observer(builder: (_) {
                  return ListView.separated(
                    shrinkWrap: true,
                    reverse: true,
                    itemCount: bois.length,
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 8),
                    itemBuilder: (context, index) {
                      var boi = bois[index];
                      return Row(children: <Widget>[
                        SizedBox(
                          width: 200,
                          child: SelectableText(
                            stampToDateAndTime(boi.timeCreated),
                            style: const TextStyle(fontSize: 18),
                          ),
                        ),
                        //

                        Expanded(
                          child: BoiSlab(
                              index: index,
                              boi: boi,
                              onTap: () {
                                Navigator.of(context).pushNamed(
                                    Routes.boiStatus,
                                    arguments: Args(boi: boi));
                              }),
                        ),
                      ]);
                    },
                  );
                }),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
