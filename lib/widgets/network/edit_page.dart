import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/shared/crumbs.dart';
import 'package:pushboy_web/widgets/shared/divider_overview.dart';

class NetworkEditPage extends StatefulWidget {
  const NetworkEditPage({required this.network});
  final Network network;

  @override
  NetworkEditPageState createState() => NetworkEditPageState();
}

class NetworkEditPageState extends State<NetworkEditPage> {
  @override
  Widget build(BuildContext context) {
    final project = King.of(context).dad.projects[widget.network.projectId];
    final network = widget.network;

    return WebScaffold(
      body: Scrollbar(
        thumbVisibility: true,
        child: SingleChildScrollView(
          child: Center(
            child: Observer(
              builder: (_) => Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Crumbs(project: project, network: widget.network),
                  const SizedBox(height: 30),

                  Row(
                    children: <Widget>[
                      const Text18('Network Name'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.networkChangeName,
                              arguments: Args(network: widget.network));
                        },
                      ),
                      const SizedBox(width: 8),
                    ],
                  ),
                  const SizedBox(height: 4),
                  Text(widget.network.name,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  Row(
                    children: <Widget>[
                      const Text18('Abbreviation'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.networkChangeAbbrev,
                              arguments: Args(network: widget.network));
                        },
                      ),
                    ],
                  ),
                  Text(widget.network.abbrev,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  Row(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const Text18('Network Subscription URL'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.networkChangeRoute,
                              arguments: Args(network: widget.network));
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 6),
                  Observer(builder: (_) {
                    final url = 'https://pushmeta.io/s/${widget.network.route}';
                    return Row(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          SelectableText(url,
                              style: const TextStyle(fontSize: 18)),
                          const SizedBox(width: 8),
                          IconButton(
                            icon: const Icon(Icons.copy, color: Colors.black),
                            onPressed: () {
                              Clipboard.setData(ClipboardData(text: url));
                              King.of(context)
                                  .snacker
                                  .addSnack(Snacks.copiedToClipboard);
                            },
                          ),
                        ]);
                  }),
                  const DividerOverview(),
                  //

                  Row(
                    children: <Widget>[
                      const Text18('Description'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.networkChangeDescription,
                              arguments: Args(network: widget.network));
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 4),
                  Text(widget.network.description,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  Row(
                    children: <Widget>[
                      const Text18('Notes'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.networkChangeNotes,
                              arguments: Args(network: widget.network));
                        },
                      ),
                    ],
                  ),
                  Text(widget.network.notes,
                      maxLines: 3,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),

                  Row(children: <Widget>[
                    const Text18('Primary Color:'),
                    const SizedBox(width: 8),
                    ColorBlock(color: network.colorPrimary),
                    const SizedBox(width: 8),
                    TextButton(
                      child: const BaText('Edit'),
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                            Routes.networkChangeColorPrimary,
                            arguments: Args(
                                network: network,
                                whichColor: WhichColor.primary));
                      },
                    ),
                  ]),
                  const SizedBox(height: 16),

                  Row(children: <Widget>[
                    const Text18('Secondary Color:'),
                    const SizedBox(width: 8),
                    ColorBlock(color: network.colorSecondary),
                    const SizedBox(width: 8),
                    TextButton(
                      child: const BaText('Edit'),
                      onPressed: () {
                        Navigator.of(context).pushNamed(
                            Routes.networkChangeColorSecondary,
                            arguments: Args(
                                network: network,
                                whichColor: WhichColor.secondary));
                      },
                    ),
                  ]),
                  const DividerOverview(),

                  Row(
                    children: <Widget>[
                      const Text18('Network thumbnail'),
                      const SizedBox(width: 8),
                      TextButton(
                        child: const BaText('Edit'),
                        onPressed: () {
                          Navigator.of(context).pushNamed(
                              Routes.networkChangeThumb,
                              arguments: Args(network: widget.network));
                        },
                      ),
                    ],
                  ),
                  const SizedBox(height: 8),
                  NetworkThumbnails(widget.network),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class NetworkThumbnails extends StatelessWidget {
  const NetworkThumbnails(this.network);
  final Network network;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(
          children: <Widget>[
            BasedFadeInImage(
              altText: network.abbrev,
              colorBackground: network.colorPrimary,
              colorText: network.colorSecondary,
              height: 80,
              width: 80,
              src: network.thumbSelectedUrl,
            ),
            const SizedBox(height: 8),
            const Text16('Current thumbnail'),
          ],
        ),
        const SizedBox(width: 20),
        //

        Column(
          children: <Widget>[
            BasedFadeInImage(
              altText: network.abbrev,
              colorBackground: network.colorPrimary,
              colorText: network.colorSecondary,
              height: 80,
              width: 80,
              src: '',
            ),
            const SizedBox(height: 8),
            const Text16('Thumbnail if there is an error.'),
          ],
        ),
      ],
    );
  }
}

class ChannelCard extends StatefulWidget {
  const ChannelCard({
    required this.channel,
    required this.index,
    required this.network,
  });
  final int index;
  final Channel channel;
  final Network network;

  @override
  ChannelCardState createState() => ChannelCardState();
}

class ChannelCardState extends State<ChannelCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(Routes.channelOverview,
              arguments:
                  Args(channel: widget.channel, network: widget.network));
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('${widget.index + 1}. ${widget.channel.name}',
                    style: const TextStyle(fontSize: 18)),
                const SizedBox(height: 4),
                BaText(widget.channel.description),
                const SizedBox(height: 4),
                BaText(widget.channel.notes),
                const SizedBox(height: 4),
                BaText('Route: ${widget.channel.route}'),
              ]),
        ),
      ),
    );
  }
}
