import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/shared/crumbs.dart';
import 'package:pushboy_web/widgets/shared/divider_overview.dart';
import 'package:pushboy_web/widgets/stat/token_box.dart';

class StatEditPage extends StatefulWidget {
  const StatEditPage({required this.channel});
  final Channel channel;

  @override
  StatEditPageState createState() => StatEditPageState();
}

class StatEditPageState extends State<StatEditPage> {
  final plan = Plan();

  @override
  Widget build(BuildContext context) {
    final king = King.of(context);
    final channel = widget.channel;
    final network = channel.getNetwork(context);
    final project =
        King.of(context).dad.projects[network.projectId] ?? Project();
    plan.loadFromApi(king, project.planId);

    return WebScaffold(
      body: Scrollbar(
        thumbVisibility: true,
        child: SingleChildScrollView(
          child: Center(
            child: Observer(builder: (_) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Crumbs(
                    project: project,
                    network: network,
                    channel: channel,
                  ),
                  const SizedBox(height: 24),
                  EditStaticTokenBox(
                    channel: channel,
                    plan: plan,
                    project: project,
                  ),

                  const SizedBox(height: 24),
                  Row(
                    children: <Widget>[
                      const Text18('Title'),
                      const SizedBox(width: 8),
                      channel.isStaticEditActivated
                          ? TextButton(
                              child: const BaText('Edit'),
                              onPressed: () {
                                Navigator.of(context).pushNamed(
                                    Routes.statChangeTitle,
                                    arguments: Args(channel: channel));
                              },
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                  Text(channel.staticTitle,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),
                  //

                  Row(
                    children: <Widget>[
                      const Text18('Body text'),
                      const SizedBox(width: 8),
                      channel.isStaticEditActivated
                          ? TextButton(
                              child: const BaText('Edit'),
                              onPressed: () {
                                Navigator.of(context).pushNamed(
                                    Routes.statChangeBody,
                                    arguments: Args(channel: channel));
                              },
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                  const Text16(
                      'The body supports the following HTML elements: b, br'),
                  const SizedBox(height: 6),
                  Text(channel.staticBody,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),
                  //

                  Row(
                    children: <Widget>[
                      const Text18('Notification links to URL'),
                      const SizedBox(width: 8),
                      channel.isStaticEditActivated
                          ? TextButton(
                              child: const BaText('Edit'),
                              onPressed: () {
                                Navigator.of(context).pushNamed(
                                    Routes.statChangeUrl,
                                    arguments: Args(channel: channel));
                              },
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                  Text(channel.staticUrl,
                      style: const TextStyle(fontSize: 18),
                      overflow: TextOverflow.ellipsis),
                  const DividerOverview(),
                  //

                  Row(
                    children: <Widget>[
                      const Text18('Thumbnail'),
                      const SizedBox(width: 8),
                      channel.isStaticEditActivated
                          ? TextButton(
                              child: const BaText('Change thumbnail'),
                              onPressed: () {
                                Navigator.of(context).pushNamed(
                                    Routes.statChangeThumbUrl,
                                    arguments: Args(
                                        channel: channel, editStatic: true));
                              },
                            )
                          : const SizedBox.shrink(),
                    ],
                  ),
                  const SizedBox(height: 12),
                  SelectableText(channel.staticThumbSelectedUrl,
                      maxLines: 2, style: const TextStyle(fontSize: 18)),
                  const SizedBox(height: 12),
                  StatThumbnails(channel),
                  const DividerOverview(),

                  const Text18('Static Notification'),
                  const SizedBox(height: 20),
                  StaticBoiSlab(channel: channel),
                ],
              );
            }),
          ),
        ),
      ),
    );
  }
}

class StatThumbnails extends StatelessWidget {
  const StatThumbnails(this.channel);
  final Channel channel;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: <Widget>[
        Column(
          children: <Widget>[
            BasedFadeInImage(
              altText: channel.abbrev,
              colorBackground: channel.colorPrimary,
              colorText: channel.colorSecondary,
              height: 80,
              width: 80,
              src: channel.staticThumbSelectedUrl,
            ),
            const SizedBox(height: 8),
            const Text16('Current thumbnail'),
          ],
        ),
        const SizedBox(width: 20),
        //

        Column(
          children: <Widget>[
            BasedFadeInImage(
              altText: channel.abbrev,
              colorBackground: channel.colorPrimary,
              colorText: channel.colorSecondary,
              height: 80,
              width: 80,
              src: '',
            ),
            const SizedBox(height: 8),
            const Text16('Thumbnail if none is set or for errors'),
          ],
        ),
      ],
    );
  }
}
