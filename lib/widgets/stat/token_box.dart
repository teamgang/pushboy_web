import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class EditStaticTokenBox extends StatefulWidget {
  const EditStaticTokenBox({
    required this.channel,
    required this.plan,
    required this.project,
  });
  final Channel channel;
  final Plan plan;
  final Project project;

  @override
  _EditStaticTokenBoxState createState() => _EditStaticTokenBoxState();
}

class _EditStaticTokenBoxState extends State<EditStaticTokenBox> {
  String _failureMsg = '';
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    final channel = widget.channel;

    return Observer(builder: (_) {
      channel.loadStaticEditTokenInfo(context);

      return channel.isStaticEditActivated
          ? Card(
              color: Colors.green,
              child: Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 20, vertical: 20),
                child: Text18(
                  'You have spent a token to edit this channel. You are able to make changes to this notification for ${channel.editStaticTimeRemaining} more hours.',
                  maxLines: 4,
                ),
              ),
            )
          : Card(
              color: Colors.orange,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 20),
                      child: Observer(
                        builder: (_) {
                          return Column(
                            children: <Widget>[
                              const SizedBox(height: 8),
                              const Text18(
                                'Static notifications are intended to be modified rarely. To make an edit, you must spend an Edit Static token. After spending a token, you will have 24 hours to make changes and test the notification. You will allowed 1 token each month for free.',
                                maxLines: 5,
                              ),
                              const SizedBox(height: 8),
                              //

                              Text18(
                                  'Your project has ${channel.statTokensPurchased} tokens remaining that you have purchased.'),
                              const SizedBox(height: 8),
                              Text18(
                                  'This channel has ${channel.statMonthlyTokensLeft} free monthly tokens remaining.'),
                              const SizedBox(height: 8),
                            ],
                          );
                        },
                      ),
                    ),
                  ),
                  const SizedBox(width: 20),
                  //

                  Column(
                    children: <Widget>[
                      const SizedBox(height: 8),
                      FunctionCard(
                          text: 'Buy a token',
                          onTap: () {
                            _showConfirmBuyModal(context);
                          }),
                      const SizedBox(height: 8),
                      FunctionCard(
                          text: 'Spend a token to edit now',
                          onTap: () {
                            _showConfirmSpendModal(context);
                          }),
                      const SizedBox(height: 8),
                      _failureMsg == ''
                          ? const SizedBox.shrink()
                          : Text(_failureMsg, style: Bast.failure),
                    ],
                  ),
                ],
              ),
            );
    });
  }

  _onBuyToken(BuildContext context) async {
    if (!_requestInProgress) {
      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.statTokenBuy,
        payload: {
          'project_id': widget.project.projectId,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        widget.project.loadFromApi(context, widget.project.projectId);
        widget.channel.loadFromApi(context, widget.channel.channelId);
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to spend token.';
        });
      }
    }
  }

  _onSpend(BuildContext context) async {
    if (!_requestInProgress) {
      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.statTokenConsume,
        payload: {
          'channel_id': widget.channel.channelId,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        widget.project.loadFromApi(context, widget.project.projectId);
        widget.channel.loadFromApi(context, widget.channel.channelId);
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to spend token.';
        });
      }
    }
  }

  void _showConfirmSpendModal(BuildContext context) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 200,
          width: 400,
          color: Colors.amber,
          child: Center(
            child: widget.channel.statMonthlyTokensLeft == 0
                ? const Text('You must buy more tokens.')
                : Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                        const Text('Are you sure you want to spend a token?'),
                        const SizedBox(height: 12),
                        Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              ElevatedButton(
                                  child: const Text('Yes'),
                                  onPressed: () {
                                    _onSpend(context);
                                  }),
                              const SizedBox(width: 60),
                              ElevatedButton(
                                child: const Text('No'),
                                onPressed: () => Navigator.pop(context),
                              ),
                            ]),
                      ]),
          ),
        );
      },
    );
  }

  void _showConfirmBuyModal(BuildContext context) {
    final plan = King.of(context).dad.getPlan(widget.project.planId);

    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Observer(builder: (_) {
          return Container(
            height: 200,
            width: 400,
            color: Colors.amber,
            child: Center(
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    Text(
                        'Are you sure you want to buy a token? This action will charge ${plan.staticEditTokenPrice.toUsd} to this project.'),
                    const SizedBox(height: 12),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          ElevatedButton(
                              child: const Text('Buy now'),
                              onPressed: () {
                                _onBuyToken(context);
                                Navigator.of(context).pop();
                              }),
                          const SizedBox(width: 60),
                          ElevatedButton(
                            child: const Text('No'),
                            onPressed: () => Navigator.pop(context),
                          ),
                        ]),
                  ]),
            ),
          );
        });
      },
    );
  }
}
