import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/console/console_page.dart';
import 'package:pushboy_web/widgets/welcome.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) =>
          King.of(context).todd.isSignedIn ? const ConsolePage() : Welcome(),
    );
  }
}
