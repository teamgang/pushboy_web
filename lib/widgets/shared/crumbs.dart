import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class Crumbs extends StatelessWidget {
  const Crumbs({this.channel, this.network, this.project});
  final Channel? channel;
  final Network? network;
  final Project? project;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        TextButton(
            child: const Text18('> Console'),
            onPressed: () {
              Navigator.of(context).pushNamed(Routes.console);
            }),
        project != null
            ? ProjectCrumb(project, isClickable: network != null)
            : const SizedBox.shrink(),
        network != null
            ? NetworkCrumb(network, isClickable: channel != null)
            : const SizedBox.shrink(),
        channel != null ? ChannelCrumb(channel) : const SizedBox.shrink(),
      ],
    );
  }
}

class LittlePad extends StatelessWidget {
  const LittlePad(this.text);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      child: Text(text,
          style: const TextStyle(fontSize: 18),
          overflow: TextOverflow.ellipsis),
    );
  }
}

class ProjectCrumb extends StatelessWidget {
  const ProjectCrumb(this.project, {required this.isClickable});
  final bool isClickable;
  final Project? project;

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      var name = project?.name ?? '';
      return isClickable
          ? TextButton(
              child: Text('> Project: $name',
                  style: const TextStyle(fontSize: 18),
                  overflow: TextOverflow.ellipsis),
              onPressed: () {
                Navigator.of(context).pushNamed(Routes.projectNetworksTab,
                    arguments: Args(project: project));
              })
          : LittlePad('> Project: $name');
    });
  }
}

class NetworkCrumb extends StatelessWidget {
  const NetworkCrumb(this.network, {required this.isClickable});
  final bool isClickable;
  final Network? network;

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) {
      var name = network?.name ?? '';
      return isClickable
          ? TextButton(
              child: Text('> Network: $name',
                  style: const TextStyle(fontSize: 18),
                  overflow: TextOverflow.ellipsis),
              onPressed: () {
                Navigator.of(context).pushNamed(Routes.networkOverview,
                    arguments: Args(network: network));
              })
          : LittlePad('> Network: $name');
    });
  }
}

class ChannelCrumb extends StatelessWidget {
  const ChannelCrumb(this.channel);
  final Channel? channel;

  @override
  Widget build(BuildContext context) {
    var name = channel?.name ?? '';

    return Observer(builder: (_) => LittlePad('> Channel: $name'));
  }
}
