import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;

import 'package:pushboy_lib/pushboy_lib.dart';

class ImageUploadPage extends StatefulWidget {
  const ImageUploadPage({
    this.boiSender,
    this.channel,
    this.editStatic = false,
    this.network,
  });
  final BoiSender? boiSender;
  final Channel? channel;
  final bool editStatic;
  final Network? network;

  @override
  ImageUploadPageState createState() => ImageUploadPageState();
}

class ImageUploadPageState extends State<ImageUploadPage> {
  String _about = '';
  bool _canSave = false;
  String _failureMsg = '';
  int _imageSizeKb = 0;
  bool _isAbbrev = false;
  bool _isUsingChan = false;
  bool _isExternal = false;
  bool _isUpload = false;
  final int _maxLength = ValidateNotification.thumbUrlMaxLength;
  final _picker = ImagePicker();
  bool _requestInProgress = false;
  final TextEditingController _textController = TextEditingController(text: '');
  String _filepath = '';

  String _abbrevThumb = '';
  String _currentChannelThumb = '';
  String _selectedThumb = '';

  String _abbrev = '';
  Color _colorBackground = Colors.grey;
  Color _colorText = Colors.blue;

  bool get _isBoi {
    return widget.boiSender != null;
  }

  bool get _isEditStatic {
    return widget.editStatic;
  }

  bool get _isChannel {
    return widget.channel != null;
  }

  bool get _isNetwork {
    return widget.network != null;
  }

  @override
  initState() {
    super.initState();
    _about =
        'The thumbnail image will be displayed to users in a thumbnail format of about 300x300 pixels. For best results, upload a square image. Images over 300 KB will fail to send to iOS devices.';

    final channel = widget.channel;
    final network = widget.network;
    final boiSender = widget.boiSender;
    if (_isEditStatic) {
      if (channel == null) {
        throw Exception('passed edit static without a channel');
      }
      _abbrevThumb = channel.thumbAbbrev;
      _currentChannelThumb = channel.thumbSelectedUrl;
      _selectedThumb = channel.staticThumbSelected;

      _abbrev = channel.abbrev;
      _colorBackground = channel.colorPrimary;
      _colorText = channel.colorSecondary;
    } else if (_isChannel) {
      _abbrevThumb = channel!.thumbAbbrev;
      _selectedThumb = channel.thumbSelected;

      _abbrev = channel.abbrev;
      _colorBackground = channel.colorPrimary;
      _colorText = channel.colorSecondary;
    } else if (_isNetwork) {
      _abbrevThumb = network!.thumbAbbrev;
      _selectedThumb = network.thumbSelected;

      _abbrev = network.abbrev;
      _colorBackground = network.colorPrimary;
      _colorText = network.colorSecondary;
    } else if (_isBoi) {
      _abbrevThumb = boiSender!.channel.thumbAbbrev;
      _currentChannelThumb = boiSender.channel.thumbSelectedUrl;
      _selectedThumb = boiSender.thumbSelected;

      _abbrev = boiSender.channel.abbrev;
      _colorBackground = boiSender.channel.colorPrimary;
      _colorText = boiSender.channel.colorSecondary;
    }

    if (_selectedThumb == 'abbrev') {
      _isAbbrev = true;
    } else if (_selectedThumb == 'chan') {
      _isUsingChan = true;
    } else if (_selectedThumb.contains('http://localhost') ||
        _selectedThumb.contains('https://pushboi.io')) {
      _isUpload = true;
      _textController.text = _selectedThumb;
    } else if (_selectedThumb.contains('http')) {
      _isExternal = true;
      _textController.text = _selectedThumb;
    } else {
      _isAbbrev = true;
      _canSave = true;
    }

    _updateImageSize();
  }

  Widget get _image {
    if (_isAbbrev) {
      return BasedFadeInImage(
        width: 200,
        height: 200,
        src: _abbrevThumb,
        altText: 'loading',
        useLoadingOnError: true,
      );
    } else if (_isUsingChan) {
      return BasedFadeInImage(
        width: 200,
        height: 200,
        src: _currentChannelThumb,
        altText: _abbrev,
        useLoadingOnError: false,
        colorBackground: _colorBackground,
        colorText: _colorText,
      );
    } else if (_isUpload) {
      return BasedFadeInImage(
        width: 200,
        height: 200,
        src: _selectedThumb,
        altText: 'loading',
        useLoadingOnError: true,
      );
    } else {
      return BasedFadeInImage(
        width: 200,
        height: 200,
        src: _selectedThumb,
        altText: 'loading',
        useLoadingOnError: true,
      );
    }
  }

  _updateImageSize() async {
    int newBytes = 0;
    if (_isUpload) {
      newBytes = await XFile(_selectedThumb).length();
    } else {
      if (_selectedThumb.isNotEmpty) {
        try {
          http.Response resp = await http.head(Uri.parse(_selectedThumb));
          newBytes = int.tryParse(resp.headers['content-length'] ?? '0') ?? 0;
        } on Exception catch (_) {
          setState(() {
            _imageSizeKb = -1;
          });
        }
      }
    }

    setState(() {
      _imageSizeKb = (newBytes / 1024).floor();
    });
  }

  Future _pickImageFromGallery() async {
    final pickedFile = await _picker.pickImage(source: ImageSource.gallery);
    setState(() {
      if (pickedFile != null) {
        _canSave = true;
        _filepath = pickedFile.path;
        _selectedThumb = _filepath;
      } else {
        _canSave = false;
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _isChannel || _isNetwork || _isBoi
              ? Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        value: _isAbbrev,
                        onChanged: (bool? newValue) {
                          setState(() {
                            _isAbbrev = true;
                            _isExternal = false;
                            _isUpload = false;
                            _isUsingChan = false;
                            _selectedThumb = 'abbrev';
                            _canSave = true;
                          });
                        },
                      ),
                      const Expanded(
                          child: Text('Use thumbnail with abbreviation')),
                    ],
                  ),
                )
              : const SizedBox.shrink(),

          _isEditStatic || _isBoi
              ? Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        value: _isUsingChan,
                        onChanged: (bool? newValue) {
                          //

                          setState(() {
                            _isAbbrev = false;
                            _isExternal = false;
                            _isUpload = false;
                            _isUsingChan = true;
                            _selectedThumb = 'chan';
                            _canSave = true;
                          });
                        },
                      ),
                      const Expanded(child: Text('Use channel\'s thumbnail')),
                    ],
                  ),
                )
              : const SizedBox.shrink(),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              children: <Widget>[
                Checkbox(
                  value: _isExternal,
                  onChanged: (bool? newValue) {
                    setState(() {
                      _isAbbrev = false;
                      _isExternal = true;
                      _isUpload = false;
                      _isUsingChan = false;
                      _selectedThumb = _textController.text;

                      if (_selectedThumb.isEmpty) {
                        _canSave = false;
                      }
                    });
                  },
                ),
                const Expanded(child: Text('Use externally hosted image')),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
            child: Row(
              children: <Widget>[
                Checkbox(
                  value: _isUpload,
                  onChanged: (bool? newValue) {
                    setState(() {
                      _isAbbrev = false;
                      _isExternal = false;
                      _isUpload = true;
                      _isUsingChan = false;
                      _selectedThumb = _filepath;

                      if (_selectedThumb.isEmpty) {
                        _canSave = false;
                      }
                    });
                  },
                ),
                const Expanded(child: Text('Use uploaded image')),
              ],
            ),
          ),

          const SizedBox(height: 10),
          _isUpload
              ? FunctionCard(
                  text: 'Select image to upload',
                  onTap: _pickImageFromGallery,
                )
              : const SizedBox.shrink(),
          const SizedBox(height: 30),

          _isExternal
              ? TextFormField(
                  autovalidateMode: AutovalidateMode.onUserInteraction,
                  controller: _textController,
                  enabled: !_requestInProgress,
                  keyboardType: TextInputType.multiline,
                  maxLength: _maxLength,
                  minLines: 1,
                  maxLines: 4,
                  style: Theme.of(context).textTheme.headline6,
                  decoration: InputDecoration(
                    border: const OutlineInputBorder(),
                    labelText: 'Thumbnail URL',
                    counterText: '${_textController.text.length}/$_maxLength',
                  ),
                  onChanged: (newValue) {
                    setState(() {
                      _selectedThumb = _textController.text;
                    });
                    if (ValidateNotification.url(newValue).isValid) {
                      setState(() {
                        _canSave = true;
                      });
                    } else {
                      setState(() {
                        _canSave = false;
                      });
                    }
                  },
                  validator: (value) {
                    return ValidateNotification.url(_textController.text)
                        .asValidator;
                  },
                )
              : const SizedBox.shrink(),

          Text18(_about, maxLines: 4),
          const SizedBox(height: 10),
          Center(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: _image,
            ),
          ),
          const SizedBox(height: 30),

          !_isNetwork && !_isUpload
              ? Text14(
                  'This image is $_imageSizeKb KB. Images greater than 300 KB do not work notifications on iOS devices.',
                  maxLines: 3)
              : const SizedBox.shrink(),
          const SizedBox(height: 8),

          _isUpload
              ? const Text14(
                  'This image will shrunk when uploaded to our server to try and keep it under the 300 KB iOS limit.',
                  maxLines: 3)
              : const SizedBox.shrink(),
          const SizedBox(height: 30),

          _requestInProgress
              ? LoadingWidget()
              : SizedBox(
                  height: 50,
                  width: 60,
                  child: ElevatedButton(
                    onPressed: _canSave
                        ? () {
                            _onSave(context);
                          }
                        : null,
                    child: const Text18('Save'),
                  ),
                ),
          const SizedBox(height: 30),
          //

          FailureMsg(_failureMsg),
        ],
      ),
    );
  }

  _onSave(BuildContext context) async {
    if (_isBoi && (_isAbbrev || _isExternal || _isUsingChan)) {
      if (_isAbbrev) {
        widget.boiSender!.thumbSelected = 'abbrev';
      } else if (_isExternal) {
        widget.boiSender!.thumbSelected = _selectedThumb;
      } else if (_isUsingChan) {
        widget.boiSender!.thumbSelected = 'chan';
      }
      Navigator.of(context).pop();
      return;
    }

    if (!_requestInProgress) {
      final king = King.of(context);
      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      var as64 = base64Encode(await XFile(_selectedThumb).readAsBytes());
      ApiResponse ares = ApiResponse();
      var payload = {
        'is_abbrev': _isAbbrev,
        'is_using_chan': _isUsingChan,
        'is_external': _isExternal,
        'is_upload': _isUpload,
        'thumb_b64': _isUpload ? Uri.encodeFull(as64) : '',
        'thumb_selected': _selectedThumb,
      };

      if (_isEditStatic) {
        payload['channel_id'] = widget.channel!.channelId;
        ares = await king.lip.api(
          EndpointsV1.statChangeThumbUrl,
          payload: payload,
        );
      } else if (_isChannel) {
        payload['channel_id'] = widget.channel!.channelId;
        ares = await king.lip.api(
          EndpointsV1.channelChangeThumb,
          payload: payload,
        );
      } else if (_isNetwork) {
        payload['network_id'] = widget.network!.networkId;
        ares = await king.lip.api(
          EndpointsV1.networkChangeThumb,
          payload: payload,
        );
      } else if (_isBoi) {
        ares = await king.lip.api(
          EndpointsV1.boiUploadThumb,
          payload: payload,
        );
      }

      this.setState(() {
        _requestInProgress = false;
      });

      imageCache.clear();
      if (ares.isOk) {
        if (_isEditStatic || _isChannel) {
          if (!mounted) return;
          widget.channel?.loadFromApi(
            context,
            widget.channel?.channelId ?? '',
          );
        } else if (_isNetwork) {
          if (!mounted) return;
          widget.network?.loadFromApi(context, widget.network?.networkId ?? '');
        } else if (_isBoi) {
          widget.boiSender!.readThumbSelected(ares.body);
        }
        if (!mounted) return;
        Navigator.of(context).pop();
      } else {
        if (ares.hasErrorMsg) {
          setState(() {
            _failureMsg = ares.firstErrorMsg;
          });
        } else {
          setState(() {
            _failureMsg = 'Failed to submit change.';
          });
        }
      }
    }
  }
}
