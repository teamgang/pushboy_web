import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/payment/pay_form/pay_form.dart';

class PayNowPage extends StatefulWidget {
  const PayNowPage({required this.bill, required this.project});
  final Bill bill;
  final Project project;

  @override
  _PayNowPageState createState() => _PayNowPageState();
}

class _PayNowPageState extends State<PayNowPage> {
  PaymentDetails paymentDetails = PaymentDetails();

  @override
  initState() {
    super.initState();
    final bill = widget.bill;
    final project = widget.project;
    paymentDetails.applyCredit = project.credit;
    paymentDetails.billId = bill.billId;
    paymentDetails.manualTotal = bill.price;
    paymentDetails.projectId = project.projectId;
    paymentDetails.silo = PaymentSilo.billPay;
  }

  int get priceAfterCredit {
    int newPrice = widget.bill.price - widget.project.credit;
    if (newPrice < 0) {
      return 0;
    } else {
      return newPrice;
    }
  }

  @override
  Widget build(BuildContext context) {
    final bill = widget.bill;
    final project = widget.project;

    return WebScaffold(
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SelectableText('Pay bill ID: ${bill.billId}'),
            const SizedBox(height: 8),
            Text18('Credit on project: ${project.credit.toUsd}'),
            const SizedBox(height: 8),
            Text18('Amount due: ${priceAfterCredit.toUsd}'),
            const SizedBox(height: 20),
            //

            PayForm(
              paymentDetails: paymentDetails,
              project: widget.project,
            )
          ]),
    );
  }
}
