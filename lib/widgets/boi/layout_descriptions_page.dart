import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/shared/divider_overview.dart';

class BoiLayoutDescriptionsPage extends StatefulWidget {
  @override
  BoiLayoutDescriptionsPageState createState() =>
      BoiLayoutDescriptionsPageState();
}

class BoiLayoutDescriptionsPageState extends State<BoiLayoutDescriptionsPage> {
  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Observer(
        builder: (_) => Column(
          mainAxisSize: MainAxisSize.max,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const SizedBox(height: 10),
            const Text20('About notification layouts'),
            const SizedBox(height: 10),
            //

            const Text16(
                'PushBoi currently supports 3 notification layouts: Standard, Big Text, and Big Picture. You must purchase Limitless Notifications in order to change the notification layout.'),
            const SizedBox(height: 20),

            const Text18('Standard Layout'),
            const SizedBox(height: 4),
            const Text18(
                'Static Notifications are sent with the Standard Layout. Most notifications from most apps are in this format.'),
            const SizedBox(height: 10),

            GridView.count(
              shrinkWrap: true,
              childAspectRatio: 1.9,
              crossAxisCount: 6,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: <Widget>[
                SizedBox(
                  width: 160,
                  height: 160,
                  child: Image.asset('assets/layouts/standard01.webp'),
                ),
                SizedBox(
                  width: 160,
                  height: 160,
                  child: Image.asset('assets/layouts/standard02.webp'),
                ),
                SizedBox(
                  width: 160,
                  height: 160,
                  child: Image.asset('assets/layouts/standard03.webp'),
                ),
              ],
            ),
            const SizedBox(height: 20),
            const DividerOverview(),

            const Text18('Big Text Layout'),
            const SizedBox(height: 4),
            const Text18(
                'Draw more attention to your notification and make it easier to read.'),
            const SizedBox(height: 10),

            GridView.count(
              shrinkWrap: true,
              childAspectRatio: 1.9,
              crossAxisCount: 6,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: <Widget>[
                SizedBox(
                  width: 160,
                  height: 160,
                  child: Image.asset('assets/layouts/bigText01.webp'),
                ),
                SizedBox(
                  width: 160,
                  height: 160,
                  child: Image.asset('assets/layouts/bigText02.webp'),
                ),
                SizedBox(
                  width: 160,
                  height: 160,
                  child: Image.asset('assets/layouts/bigText03.webp'),
                ),
              ],
            ),
            const SizedBox(height: 20),
            const DividerOverview(),

            const Text18('Big Picture Layout'),
            const SizedBox(height: 4),
            const Text18('Maximizes engagement with a popping picture.'),
            const SizedBox(height: 10),

            GridView.count(
              shrinkWrap: true,
              childAspectRatio: 1.9,
              crossAxisCount: 6,
              crossAxisSpacing: 10,
              mainAxisSpacing: 10,
              children: <Widget>[
                SizedBox(
                  width: 160,
                  height: 160,
                  child: Image.asset('assets/layouts/bigPicture01.webp'),
                ),
                SizedBox(
                  width: 160,
                  height: 160,
                  child: Image.asset('assets/layouts/bigPicture02.webp'),
                ),
                SizedBox(
                  width: 160,
                  height: 160,
                  child: Image.asset('assets/layouts/bigPicture03.webp'),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
