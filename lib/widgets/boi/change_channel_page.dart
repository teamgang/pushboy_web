import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class BoiChangeChannelPage extends StatefulWidget {
  const BoiChangeChannelPage({
    required this.boiSender,
    required this.network,
  });
  final BoiSender boiSender;
  final Network network;

  @override
  _BoiChangeChannelPageState createState() => _BoiChangeChannelPageState();
}

class _BoiChangeChannelPageState extends State<BoiChangeChannelPage> {
  @override
  Widget build(BuildContext context) {
    final network = widget.network;
    final channels =
        King.of(context).dad.channels.getChannelsOfNetwork(network.networkId);

    return WebScaffold(
      body: Observer(
        builder: (_) {
          return ListView.separated(
            shrinkWrap: true,
            itemCount: channels.length,
            separatorBuilder: (context, index) => const SizedBox(height: 12),
            itemBuilder: (context, index) {
              return ChannelCard(
                boiSender: widget.boiSender,
                channel: channels[index],
                index: index,
              );
            },
          );
        },
      ),
    );
  }
}

class ChannelCard extends StatelessWidget {
  const ChannelCard({
    required this.boiSender,
    required this.channel,
    required this.index,
  });
  final BoiSender boiSender;
  final Channel channel;
  final int index;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
        onTap: () {
          boiSender.body = channel.staticBody;
          boiSender.title = channel.staticTitle;
          boiSender.thumbSelected = 'chan';
          boiSender.url = channel.staticUrl;
          boiSender.channel.loadFromAltChannel(channel);

          boiSender.bodyController.text = boiSender.body;
          boiSender.titleController.text = boiSender.title;
          boiSender.urlController.text = boiSender.url;

          Navigator.of(context).pop();
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('${index + 1}. ${channel.name}',
                    style: const TextStyle(fontSize: 18)),
                const SizedBox(height: 4),
                BaText(channel.description),
                const SizedBox(height: 4),
                BaText(channel.notes),
                const SizedBox(height: 4),
                BaText('Route: ${channel.route}'),
              ]),
        ),
      ),
    );
  }
}
