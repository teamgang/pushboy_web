import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class BoiStatusPage extends StatefulWidget {
  const BoiStatusPage({required this.boi});
  final Boi boi;

  @override
  BoiStatusPageState createState() => BoiStatusPageState();
}

class BoiStatusPageState extends State<BoiStatusPage> {
  String _failureMsg = '';
  bool _requestInProgress = false;

  _checkForUpdates(BuildContext context) async {
    while (widget.boi.title.isEmpty) {
      await Future.delayed(const Duration(milliseconds: 3000))
          .then((value) => widget.boi.loadFromApi(context, widget.boi.boiId));
    }
  }

  @override
  initState() {
    super.initState();
    _checkForUpdates(context);
  }

  Boi get _boi {
    return widget.boi;
  }

  @override
  Widget build(BuildContext context) {
    final boi = widget.boi;

    return WebScaffold(
      body: Observer(
        builder: (_) => Scrollbar(
          child: SingleChildScrollView(
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const Text22('Notification details'),
                const SizedBox(height: 10),
                const Text18('Title'),
                Text18(boi.title),
                const SizedBox(height: 10),
                const Text18('Body'),
                Text18(boi.body, maxLines: 80),
                const SizedBox(height: 10),
                const Text18('URL to launch'),
                Text18(boi.url),
                const SizedBox(height: 10),
                const Text18('Status'),
                Text18(boi.status),
                const SizedBox(height: 10),

                const Text18('Notification ID'),
                SelectableText(boi.boiId),
                const SizedBox(height: 4),

                const Text18('Token used'),
                Text18(boi.token),
                const SizedBox(height: 10),
                const Text18('Notification Type'),
                Text18(boi.plasticity.asString),
                const SizedBox(height: 10),
                SimpleRow(
                    left: 'Notifications attempted',
                    right: '${boi.countSuccess}'),
                const SizedBox(height: 4),
                SimpleRow(
                    left: 'Successful notifications',
                    right: '${boi.countSuccess}'),
                const SizedBox(height: 4),
                SimpleRow(
                    left: 'Failed notifications', right: '${boi.countFailure}'),

                SimpleRow(
                    left: 'Notifications delivered to status bar',
                    right: '${boi.cmCreated}'),
                SimpleRow(
                    left: 'Notifications displayed in the status bar',
                    right: '${boi.cmDisplayed}'),
                SimpleRow(
                    left: 'Notifications dismissed from status bar',
                    right: '${boi.cmDismissed}'),
                SimpleRow(
                    left: 'Notifications tapped from status bar (Impressions)',
                    right: '${boi.cmTapped}'),
                SimpleRow(
                    left:
                        'Notifications viewed in the PushBoi app (similar to impressions)',
                    right: '${boi.cmViewedInApp}'),

                const SizedBox(height: 4),
                const Text(
                    'Notifications usually fail for trivial reasons, such as an uninstalled app. You are only billed for successful notifications.'),
                const SizedBox(height: 30),
                //

                SizedBox(
                  width: 600,
                  child: BoiSlab(boi: boi),
                ),
                const SizedBox(height: 20),

                Text(widget.boi.isHidden
                    ? 'Notification is hidden'
                    : 'Notification is visible'),
                const SizedBox(height: 4),

                FunctionCard(
                    text: _boi.isHidden
                        ? 'Do not hide this notification'
                        : 'Hide this notification from users',
                    onTap: () {
                      _toggleHidden(context);
                    }),
                const SizedBox(height: 4),

                FailureMsg(_failureMsg),
              ],
            ),
          ),
        ),
      ),
    );
  }

  _toggleHidden(BuildContext context) async {
    if (!_requestInProgress) {
      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.boiHide,
        payload: {
          'boi_id': widget.boi.boiId,
          'is_hidden': !widget.boi.isHidden,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        if (!mounted) return;
        widget.boi.loadFromApi(
          context,
          widget.boi.boiId,
        );
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
