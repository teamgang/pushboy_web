import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class SendCustomBoi extends StatefulWidget {
  const SendCustomBoi({required this.boiSender});
  final BoiSender boiSender;

  @override
  SendCustomBoiState createState() => SendCustomBoiState();
}

class SendCustomBoiState extends State<SendCustomBoi> {
  final int _bodyMaxLength = ValidateNotification.bodyMaxLength;
  final int _notesMaxLength = ValidateNotification.notesMaxLength;
  final int _titleMaxLength = ValidateNotification.titleMaxLength;
  final int _urlMaxLength = ValidateNotification.urlMaxLength;

  @override
  Widget build(BuildContext context) {
    final boiSender = widget.boiSender;
    return Observer(
      builder: (_) => Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            controller: boiSender.titleController,
            enabled: !boiSender.requestInProgress,
            keyboardType: TextInputType.multiline,
            maxLength: _titleMaxLength,
            minLines: 1,
            maxLines: 3,
            style: Theme.of(context).textTheme.headline6,
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              labelText: 'Title text',
              counterText: '${boiSender.title.length}/$_titleMaxLength',
            ),
            onChanged: (newValue) {
              boiSender.title = newValue;
              setState(() {});
            },
            validator: (value) {
              return ValidateNotification.title(boiSender.title).asValidator;
            },
          ),
          const SizedBox(height: 10),
          //

          TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            controller: boiSender.bodyController,
            enabled: !boiSender.requestInProgress,
            keyboardType: TextInputType.multiline,
            maxLength: _bodyMaxLength,
            minLines: 1,
            maxLines: 3,
            style: Theme.of(context).textTheme.headline6,
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              labelText: 'Body text',
              counterText: '${boiSender.body.length}/$_bodyMaxLength',
            ),
            onChanged: (newValue) {
              boiSender.body = newValue;
              setState(() {});
            },
            validator: (value) {
              return ValidateNotification.body(boiSender.body).asValidator;
            },
          ),
          const SizedBox(height: 10),

          TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            controller: boiSender.urlController,
            enabled: !boiSender.requestInProgress,
            keyboardType: TextInputType.multiline,
            maxLength: _urlMaxLength,
            minLines: 1,
            maxLines: 3,
            style: Theme.of(context).textTheme.headline6,
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              labelText: 'URL to open on click',
              counterText: '${boiSender.url.length}/$_urlMaxLength',
            ),
            onChanged: (newValue) {
              boiSender.url = newValue;
              setState(() {});
            },
            validator: (value) {
              return ValidateNotification.url(boiSender.url).asValidator;
            },
          ),
          const SizedBox(height: 10),

          TextFormField(
            autovalidateMode: AutovalidateMode.onUserInteraction,
            enabled: !boiSender.requestInProgress,
            keyboardType: TextInputType.multiline,
            maxLength: _notesMaxLength,
            minLines: 1,
            maxLines: 3,
            style: Theme.of(context).textTheme.headline6,
            decoration: InputDecoration(
              border: const OutlineInputBorder(),
              labelText: 'Notes (not shown to users)',
              counterText: '${boiSender.notes.length}/$_notesMaxLength',
            ),
            onChanged: (newValue) {
              boiSender.notes = newValue;
              setState(() {});
            },
            validator: (value) {
              return ValidateNotification.notes(boiSender.notes).asValidator;
            },
          ),
          const SizedBox(height: 10),

          FunctionCard(
              text: 'Change thumbnail',
              onTap: () async {
                await Navigator.of(context).pushNamed(Routes.boiChangeThumbUrl,
                    arguments: Args(boiSender: boiSender));
              }),
        ],
      ),
    );
  }
}
