import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class SendLimitlessBoi extends StatefulWidget {
  const SendLimitlessBoi({required this.boiSender});
  final BoiSender boiSender;

  @override
  SendLimitlessBoiState createState() => SendLimitlessBoiState();
}

class SendLimitlessBoiState extends State<SendLimitlessBoi> {
  @override
  Widget build(BuildContext context) {
    final boiSender = widget.boiSender;
    return Observer(
      builder: (_) => Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          const SizedBox(height: 10),
          const Text20('Notification layout'),
          TextButton(
            child: const BaText('Read about push notification layouts'),
            onPressed: () {
              Navigator.of(context).pushNamed(Routes.boiLayoutDescriptions,
                  arguments: Args(boiSender: boiSender));
            },
          ),
          //
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: <Widget>[
                Checkbox(
                  value: boiSender.layout == BoiLayout.default_,
                  onChanged: (bool? newValue) =>
                      boiSender.layout = BoiLayout.default_,
                ),
                const Expanded(child: Text('Standard')),
              ],
            ),
          ),
          //

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: <Widget>[
                Checkbox(
                  value: boiSender.layout == BoiLayout.bigText,
                  onChanged: (bool? newValue) =>
                      boiSender.layout = BoiLayout.bigText,
                ),
                const Expanded(child: Text('Big Text')),
              ],
            ),
          ),

          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Row(
              children: <Widget>[
                Checkbox(
                  value: boiSender.layout == BoiLayout.bigPicture,
                  onChanged: (bool? newValue) =>
                      boiSender.layout = BoiLayout.bigPicture,
                ),
                const Expanded(child: Text('Big Picture')),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
