import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/boi/send_custom.dart';
import 'package:pushboy_web/widgets/boi/send_limitless.dart';
import 'package:pushboy_web/widgets/shared/divider_overview.dart';

class BoiSendPage extends StatefulWidget {
  const BoiSendPage({required this.network});
  final Network network;

  @override
  BoiSendPageState createState() => BoiSendPageState();
}

class BoiSendPageState extends State<BoiSendPage> {
  String _about = '';
  BoiSender boiSender = BoiSender();
  String _failureMsg = '';
  bool _isCustom = false;
  bool _isLimitless = false;
  bool _isStatic = false;

  @override
  initState() {
    super.initState();
    //_urlCon.text = widget.channel?.thumbUrl ?? '';
  }

  void _setCustom() {
    boiSender.plasticity = BoiPlasticity.custom;
    setState(() {
      _isCustom = true;
      _isLimitless = false;
      _isStatic = false;
      _about =
          'Use the form below to customize your message. Your account will be charged automatically, if necessary.';
    });
  }

  void _setLimitless() {
    boiSender.plasticity = BoiPlasticity.custom;
    setState(() {
      _isCustom = false;
      _isLimitless = true;
      _isStatic = false;
      _about =
          'Use the form below to customize your message. Your account will be charged automatically, if necessary.';
    });
  }

  void _setStatic() {
    boiSender.plasticity = BoiPlasticity.static_;
    setState(() {
      _isCustom = false;
      _isLimitless = false;
      _isStatic = true;
      _about =
          'You will send the static message configured to this channel. Your account will be charged automatically, if necessary.';
    });
  }

  bool get _isPlasticitySelected {
    return _isCustom || _isLimitless || _isStatic;
  }

  bool get _isChannelSelected {
    return boiSender.channel.channelId != '';
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Scrollbar(
        thumbVisibility: true,
        child: SingleChildScrollView(
          child: Observer(
            builder: (_) => Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                const Text20('What type of notification are you sendint?'),
                const SizedBox(height: 14),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        value: _isStatic,
                        onChanged: (bool? newValue) => _setStatic(),
                      ),
                      const Expanded(child: Text('Static')),
                    ],
                  ),
                ),
                const SizedBox(height: 8),
                //

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        value: _isCustom,
                        onChanged: (bool? newValue) => _setCustom(),
                      ),
                      const Expanded(child: Text('Custom')),
                    ],
                  ),
                ),
                const SizedBox(height: 8),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    children: <Widget>[
                      Checkbox(
                        value: _isLimitless,
                        onChanged: (bool? newValue) => _setLimitless(),
                      ),
                      const Expanded(child: Text('Limitless')),
                    ],
                  ),
                ),

                _isPlasticitySelected ? _step2() : const SizedBox.shrink(),

                _isChannelSelected ? _step3() : const SizedBox.shrink(),

                const SizedBox(height: 30),
                FailureMsg(_failureMsg)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _step2() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        const DividerOverview(),
        FunctionCard(
            text: 'Select Channel',
            onTap: () {
              Navigator.of(context)
                  .pushNamed(Routes.boiChangeChannel,
                      arguments:
                          Args(boiSender: boiSender, network: widget.network))
                  .then((value) {
                setState(() {});
                imageCache.clear();
              });
            }),
        const SizedBox(height: 18),
      ],
    );
  }

  Widget _step3() {
    return Observer(
      builder: (_) => Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text18(_about, maxLines: 8),
          const DividerOverview(),
          _isCustom || _isLimitless
              ? SendCustomBoi(boiSender: boiSender)
              : const SizedBox.shrink(),
          const SizedBox(height: 30),
          //

          _isLimitless
              ? SendLimitlessBoi(boiSender: boiSender)
              : const SizedBox.shrink(),
          const SizedBox(height: 30),

          SizedBox(
            width: 600,
            child: BoiSenderSlab(boiSender: boiSender),
          ),
          const SizedBox(height: 30),

          Text18(boiSender.fcmPayloadSizeEstimateInfo),
          const SizedBox(height: 4),
          boiSender.fcmPayloadTooLarge
              ? const FailureMsg(
                  'Warning: This message cannot be sent because the payload is too large. Try to use a shorter URL link, thumb URL, or body.')
              : const SizedBox.shrink(),
          const SizedBox(height: 30),

          boiSender.requestInProgress
              ? LoadingWidget()
              : SizedBox(
                  height: 50,
                  width: 60,
                  child: ElevatedButton(
                    onPressed: boiSender.canSend
                        ? () {
                            _onSend();
                          }
                        : null,
                    child: const Text18('Send'),
                  ),
                ),
        ],
      ),
    );
  }

  Future<void> _onSend() async {
    if (!boiSender.requestInProgress) {
      this.setState(() {
        _failureMsg = '';
        boiSender.requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
            EndpointsV1.devNotificationSend,
            payload: boiSender.payload,
          );

      this.setState(() {
        boiSender.requestInProgress = false;
      });
      if (ares.isOk) {
        final boi = Boi();
        boi.boiId = ares.body['boi_id'];
        if (!mounted) return;
        Navigator.of(context)
            .pushNamed(Routes.boiStatus, arguments: Args(boi: boi));
        imageCache.clear();
      } else {
        setState(() {
          _failureMsg = ares.peekErrorMsg(defaultText: 'Failed to send.');
        });
      }
    }
  }
}
