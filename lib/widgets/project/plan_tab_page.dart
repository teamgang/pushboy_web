import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/project/project_wrapper.dart';

class ProjectPlanTabPage extends StatefulWidget {
  const ProjectPlanTabPage({required this.project});
  final Project project;

  @override
  _ProjectPlanTabPageState createState() => _ProjectPlanTabPageState();
}

class _ProjectPlanTabPageState extends State<ProjectPlanTabPage> {
  @override
  Widget build(BuildContext context) {
    var king = King.of(context);
    var project = widget.project;

    final currentPlan = king.dad.currentPlan;
    currentPlan.loadFromApi(king, project.planId);

    return ProjectWrapper(
      initialTab: 3,
      project: project,
      child: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(height: 20),

            Observer(builder: (_) {
              return Table(
                //border: TableBorder.symmetric(
                //inside: const BorderSide(width: 1, color: Colors.blue),
                //),
                defaultVerticalAlignment: TableCellVerticalAlignment.middle,
                columnWidths: const <int, TableColumnWidth>{
                  //0: IntrinsicColumnWidth(),
                  0: IntrinsicColumnWidth(),
                  1: FlexColumnWidth(),
                  //4: FixedColumnWidth(64),
                },
                children: <TableRow>[
                  TableRow(
                    decoration: const BoxDecoration(
                      border: Border(
                        bottom: BorderSide(color: Colors.blue),
                      ),
                    ),
                    children: <Widget>[
                      const LocalCell(''),
                      ColumnTitle(currentPlan.title),
                    ],
                  ),
                  //

                  TableRow(
                    children: <Widget>[
                      const RowTitle('Monthly price'),
                      LocalCell(currentPlan.priceForDisplayTable),
                    ],
                  ),
                  //

                  TableRow(
                    decoration: BoxDecoration(color: Colors.grey[200]),
                    children: const <Widget>[
                      RowTitle('Subscriber limit'),
                      LocalCell('unlimited', isItalic: true),
                    ],
                  ),
                  //

                  const TableRow(
                    children: <Widget>[
                      RowTitle('Channel limit'),
                      LocalCell('unlimited', isItalic: true),
                    ],
                  ),
                  //

                  TableRow(
                    decoration: BoxDecoration(color: Colors.grey[200]),
                    children: <Widget>[
                      const RowTitle('Monthly free Static* notifications'),
                      LocalCell('${currentPlan.monthlyFreeStatic}'),
                    ],
                  ),
                  //

                  TableRow(
                    decoration: BoxDecoration(color: Colors.grey[200]),
                    children: <Widget>[
                      const RowTitle('Monthly free Custom** notifications'),
                      LocalCell('${currentPlan.monthlyFreeCustom}'),
                    ],
                  ),
                  //

                  TableRow(
                    children: <Widget>[
                      const RowTitle(
                          'Monthly free Limitless*** notificiations'),
                      LocalCell('${currentPlan.monthlyFreeLimitless}'),
                    ],
                  ),
                  //

                  TableRow(
                    children: <Widget>[
                      Column(children: const <Widget>[
                        SizedBox(height: 6),
                        RowTitle('Additional Notifications:'),
                        RowTitle('  - Static'),
                      ]),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.bottom,
                        child: LocalCell(currentPlan.displayPriceStatic),
                      ),
                    ],
                  ),
                  //

                  TableRow(
                    decoration: BoxDecoration(color: Colors.grey[200]),
                    children: <Widget>[
                      const RowTitle('  - Custom'),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.bottom,
                        child: LocalCell(currentPlan.displayPriceCustom),
                      ),
                    ],
                  ),
                  //

                  TableRow(
                    decoration: BoxDecoration(color: Colors.grey[200]),
                    children: <Widget>[
                      const RowTitle('  - Limitless'),
                      TableCell(
                        verticalAlignment: TableCellVerticalAlignment.bottom,
                        child: LocalCell(currentPlan.displayPriceLimitless),
                      ),
                    ],
                  ),
                  //
                ],
              );
            }),
            const SizedBox(height: 20),
            //

            const Text(
              '* Static notifications allow custom images and text, but there is a fee to make alterations.',
              style: TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
            ),
            const SizedBox(height: 4),
            const Text(
              '** Custom notifications allow custom image and text for every notification. The notification will have a popup.',
              style: TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
            ),
            const SizedBox(height: 4),
            const Text(
              '*** Limitless notifications allow custom text, images, sounds, and layouts. The notification will have a popup.',
              style: TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
            ),
            const SizedBox(height: 4),
            const Text(
              'Unused notifications do not expire.',
              style: TextStyle(fontSize: 18, fontStyle: FontStyle.italic),
            ),
          ],
        ),
      ),
    );
  }
}

class ColumnTitle extends StatelessWidget {
  const ColumnTitle(this.text);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      padding: const EdgeInsets.symmetric(horizontal: 6, vertical: 4),
      child: Text(
        text,
        style: const TextStyle(
          fontSize: 20,
          //fontWeight: FontWeight.w700,
        ),
      ),
    );
  }
}

class RowTitle extends StatelessWidget {
  const RowTitle(this.text);
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerLeft,
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Text(
        text,
        style: const TextStyle(fontSize: 18),
      ),
    );
  }
}

class LocalCell extends StatelessWidget {
  const LocalCell(
    this.text, {
    this.isItalic = false,
  });
  final bool isItalic;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.centerRight,
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 8),
      child: Text(
        text,
        style: isItalic
            ? const TextStyle(
                fontSize: 16,
                fontStyle: FontStyle.italic,
              )
            : const TextStyle(fontSize: 18),
      ),
    );
  }
}
