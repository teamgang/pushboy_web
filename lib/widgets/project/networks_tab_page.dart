import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/project/project_wrapper.dart';

class ProjectNetworksTabPage extends StatefulWidget {
  const ProjectNetworksTabPage({required this.project});
  final Project project;

  @override
  ProjectNetworksTabPageState createState() => ProjectNetworksTabPageState();
}

class ProjectNetworksTabPageState extends State<ProjectNetworksTabPage> {
  Project get _project {
    return widget.project;
  }

  @override
  Widget build(BuildContext context) {
    final networks =
        King.of(context).dad.networks.getNetworksOfProject(_project.projectId);

    return ProjectWrapper(
      project: _project,
      initialTab: 0,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Observer(
                      builder: (_) => Text18('Networks (${networks.length}):')),
                ),
                IconButton(
                  icon: const Icon(Icons.refresh),
                  onPressed: () {
                    King.of(context)
                        .dad
                        .networks
                        .refreshNetworksOfProject(_project.projectId);
                  },
                ),
                const Expanded(child: SizedBox.shrink()),
                TextButton(
                  child: const Text18('+ Create a network'),
                  onPressed: () async {
                    await Navigator.of(context).pushNamed(Routes.networkCreate,
                        arguments: Args(project: _project));
                    if (!mounted) return;
                    King.of(context)
                        .dad
                        .networks
                        .getNetworksOfProjectFromApi(_project.projectId);
                  },
                ),
              ]),
          const SizedBox(height: 20),
          Observer(builder: (_) {
            return networks.isEmpty
                ? const Text('No networks found.')
                : ListView.separated(
                    shrinkWrap: true,
                    itemCount: networks.length,
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 20),
                    itemBuilder: (context, index) {
                      var network = networks[index];
                      return NetworkCard(index: index, network: network);
                    },
                  );
          }),
        ],
      ),
    );
  }
}

class NetworkCard extends StatelessWidget {
  const NetworkCard({required this.index, required this.network});
  final int index;
  final Network network;

  @override
  Widget build(BuildContext context) {
    String apiUrl = 'https://pushboi.io/s/';

    return Card(
      elevation: 2,
      child: InkWell(
        onTap: () {
          Navigator.of(context).pushNamed(Routes.networkOverview,
              arguments: Args(network: network));
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Text('${index + 1}. ${network.name}',
                    style: const TextStyle(fontSize: 18),
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis),
                const SizedBox(height: 8),
                Text(
                  'Description: ${network.description}',
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 8),
                Text(
                  'Notes: ${network.notes}',
                  maxLines: 3,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(fontSize: 16),
                ),
                const SizedBox(height: 8),
                Row(children: <Widget>[
                  SelectableText(
                    'Subscription URL: $apiUrl${network.route}',
                    style: const TextStyle(fontSize: 16),
                  ),
                ]),
              ]),
        ),
      ),
    );
  }
}
