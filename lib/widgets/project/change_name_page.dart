import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ProjectChangeNamePage extends StatefulWidget {
  const ProjectChangeNamePage({required this.project});
  final Project project;

  @override
  _ProjectChangeNamePageState createState() => _ProjectChangeNamePageState();
}

class _ProjectChangeNamePageState extends State<ProjectChangeNamePage> {
  bool _canSave = false;
  String _currentValue = '';
  String _failureMsg = '';
  final int _maxLength = ValidateProject.nameMaxLength;
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                initialValue: widget.project.name,
                keyboardType: TextInputType.multiline,
                maxLength: _maxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Name',
                  counterText: '${_currentValue.length}/$_maxLength',
                ),
                onChanged: (newValue) {
                  _currentValue = newValue;
                  if (ValidateProject.name(_currentValue).isValid) {
                    setState(() {
                      _canSave = true;
                    });
                  } else {
                    setState(() {
                      _canSave = false;
                    });
                  }
                },
                validator: (value) {
                  return ValidateProject.name(_currentValue).asValidator;
                },
              ),

              const SizedBox(height: 12),
              Text('Change the name of this project.',
                  style: optionInfoStyle()),
              const SizedBox(height: 12),
              _requestInProgress
                  ? LoadingWidget()
                  : SizedBox(
                      height: 50,
                      width: 60,
                      child: ElevatedButton(
                        child: const Text18('Save'),
                        onPressed: _canSave
                            ? () {
                                FocusScope.of(context).unfocus();
                                _onSubmit(context);
                              }
                            : null,
                      ),
                    ),
              const SizedBox(height: 12),
              _failureMsg == ''
                  ? const SizedBox.shrink()
                  : Text(_failureMsg, style: failureStyle()),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      if (_currentValue == widget.project.name) {
        Navigator.of(context).pop();
      }

      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.projectChangeName,
        payload: {
          'project_id': widget.project.projectId,
          'name': _currentValue,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        widget.project.loadFromApi(context, widget.project.projectId);
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
