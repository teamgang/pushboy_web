import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ProjectCreatePage extends StatefulWidget {
  @override
  ProjectCreatePageState createState() => ProjectCreatePageState();
}

class ProjectCreatePageState extends State<ProjectCreatePage> {
  bool _canSave = false;
  String _failureMsg = '';
  late FocusNode _focusNode;
  bool _requestInProgress = false;
  String _notes = '';
  String _name = '';

  @override
  initState() {
    super.initState();
    _focusNode = FocusNode();
    _focusNode.requestFocus();
  }

  @override
  dispose() {
    _focusNode.dispose();
    super.dispose();
  }

  void _onAnyChange() {
    if (ValidateProject.allFields(
      notes: _notes,
      name: _name,
    )) {
      setState(() {
        _canSave = true;
      });
    } else {
      setState(() {
        _canSave = false;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Padding(
        padding: const EdgeInsets.all(24.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            //

            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              enabled: !_requestInProgress,
              focusNode: _focusNode,
              keyboardType: TextInputType.multiline,
              maxLength: ValidateProject.nameMaxLength,
              style: Theme.of(context).textTheme.headline6,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: 'Name',
                counterText: '${_name.length}/${ValidateProject.nameMaxLength}',
              ),
              onChanged: (newValue) {
                _name = newValue;
                _onAnyChange();
              },
              validator: (value) {
                return ValidateProject.name(_name).asValidator;
              },
            ),
            //

            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              enabled: !_requestInProgress,
              keyboardType: TextInputType.multiline,
              maxLength: ValidateProject.notesMaxLength,
              style: Theme.of(context).textTheme.headline6,
              decoration: InputDecoration(
                border: const OutlineInputBorder(),
                labelText: 'Notes',
                counterText:
                    '${_notes.length}/${ValidateProject.notesMaxLength}',
              ),
              onChanged: (newValue) {
                _notes = newValue;
                _onAnyChange();
              },
              validator: (value) {
                return ValidateProject.notes(_notes).asValidator;
              },
            ),
            //

            const SizedBox(height: 12),
            _failureMsg == ''
                ? const SizedBox(height: 0)
                : Text(_failureMsg, style: failureStyle()),
            const SizedBox(height: 12),
            Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  TextButton(
                      onPressed: () {
                        Navigator.of(context).pop();
                      },
                      child: const Text('Cancel')),
                  TextButton(
                      onPressed: _canSave ? () => _onSubmit(context) : null,
                      child: const Text('Create')),
                ]),
          ],
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    var king = King.of(context);
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      var payload = {
        'name': _name,
        'notes': _notes,
      };

      ApiResponse ares = await king.lip.api(
        EndpointsV1.projectCreate,
        payload: payload,
      );

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        Navigator.of(context).pushNamed(Routes.console);
      } else {
        setState(() {
          _failureMsg = 'Failed to create project.';
        });
      }
    }
  }
}
