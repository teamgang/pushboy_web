import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/project/project_wrapper.dart';

class ProjectAlertsTabPage extends StatefulWidget {
  const ProjectAlertsTabPage({required this.project});
  final Project project;

  @override
  _ProjectAlertsTabPageState createState() => _ProjectAlertsTabPageState();
}

class _ProjectAlertsTabPageState extends State<ProjectAlertsTabPage> {
  @override
  Widget build(BuildContext context) {
    return ProjectWrapper(
      project: widget.project,
      initialTab: 1,
      child: Column(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                const Padding(
                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                  child: Text('Alerts'),
                ),
                IconButton(
                  icon: const Icon(Icons.refresh),
                  onPressed: () {},
                ),
                const Expanded(child: SizedBox.shrink()),
                TextButton(
                  child: const Text('+ Create a network'),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.networkCreate,
                        arguments: Args(project: widget.project));
                  },
                ),
              ]),
          const SizedBox(height: 20),
        ],
      ),
    );
  }
}
