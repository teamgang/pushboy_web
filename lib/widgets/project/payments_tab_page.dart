import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/project/project_wrapper.dart';

class ProjectPaymentsTabPage extends StatefulWidget {
  const ProjectPaymentsTabPage({
    required this.project,
  });
  final Project project;

  @override
  _ProjectPaymentsTabPageState createState() => _ProjectPaymentsTabPageState();
}

class _ProjectPaymentsTabPageState extends State<ProjectPaymentsTabPage> {
  final DateTime _now = DateTime.now();
  late DateTime _nextPaymentDate;
  final currentYear = DateTime.now().year;
  final currentMonth = DateTime.now().month;

  @override
  initState() {
    super.initState();
    if (_now.day > 14) {
      _nextPaymentDate = DateTime(currentYear, currentMonth + 1, 14);
    } else {
      _nextPaymentDate = DateTime(currentYear, currentMonth, 14);
    }
  }

  @override
  Widget build(BuildContext context) {
    var date = '2022';
    var king = King.of(context);

    final project = widget.project;
    final plan = king.dad.getPlan(project.planId);

    return ProjectWrapper(
      initialTab: 4,
      project: project,
      child: Center(
        child: Observer(
          builder: (_) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const SizedBox(height: 20),
              Text('Billing Summary', style: king.theme.styles.titleMedium),
              const Divider(color: Colors.black),
              SimpleRow(left: 'Project credit', right: project.credit.toUsd),
              SimpleRow(left: 'Current plan:', right: plan.title),
              SimpleRow(
                  left: 'Next payment due on:',
                  right: DateFormat.yMMMd().format(_nextPaymentDate)),
              SimpleRow(
                  left: 'Autopay is:', right: project.isAutopayEnabledString),
              SimpleRow(left: 'Autopayment method:', right: date),
              //

              const SizedBox(height: 20),
              Text('Notifcations Summary',
                  style: king.theme.styles.titleMedium),
              const Divider(color: Colors.black),
              SimpleRow(
                  left: 'Static bought:', right: project.staticBought.display),
              SimpleRow(
                  left: 'Static sent:', right: project.staticSent.display),
              SimpleRow(
                  left: 'Static remaining:',
                  right: project.staticRemaining.display),
              const SizedBox(height: 16),

              SimpleRow(
                  left: 'Custom bought:', right: project.customBought.display),
              SimpleRow(
                  left: 'Custom sent:', right: project.customSent.display),
              SimpleRow(
                  left: 'Custom remaining:',
                  right: project.customRemaining.display),
              const SizedBox(height: 16),

              SimpleRow(
                  left: 'Limitless bought:',
                  right: project.limitlessBought.display),
              SimpleRow(
                  left: 'Limitless sent:',
                  right: project.limitlessSent.display),
              SimpleRow(
                  left: 'Limitless remaining:',
                  right: project.limitlessRemaining.display),
              const SizedBox(height: 16),

              SimpleRow(
                  left: 'Sum of network subscritions:',
                  right: project.subscribers.display),
              //

              const SizedBox(height: 24),
              const Text18(
                  'Credit is automatically applied to your balance and future payments.'),
              const SizedBox(height: 8),
              FunctionCard(
                  text: 'Add credit with card or crypto',
                  onTap: () {
                    Navigator.of(context).pushNamed(Routes.paymentAddCredit,
                        arguments: Args(project: project));
                  }),
              const SizedBox(height: 12),

              FunctionCard(
                text: 'Manage autopay',
                onTap: () {
                  Navigator.of(context).pushNamed(Routes.paymentAutopay,
                      arguments: Args(project: project));
                },
              ),
              const SizedBox(height: 12),

              FunctionCard(
                text: 'Payment History',
                onTap: () {
                  Navigator.of(context).pushNamed(Routes.paymentsView,
                      arguments: Args(project: project));
                },
              ),
              const SizedBox(height: 12),
              //

              FunctionCard(
                text: 'View Bills',
                onTap: () {
                  Navigator.of(context).pushNamed(Routes.billsView,
                      arguments: Args(project: project));
                },
              ),
              const SizedBox(height: 12),

              FunctionCard(
                text: 'Show Billing Activity',
                onTap: () {
                  Navigator.of(context).pushNamed(Routes.billingActivity,
                      arguments: Args(project: project));
                },
              ),
              //
            ],
          ),
        ),
      ),
    );
  }
}
