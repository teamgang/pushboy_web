import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/project/project_wrapper.dart';
import 'package:pushboy_web/widgets/shared/divider_overview.dart';

class ProjectSettingsTabPage extends StatelessWidget {
  const ProjectSettingsTabPage({required this.project});
  final Project project;

  @override
  Widget build(BuildContext context) {
    //final project = King.of(context).dad.projects[project.projectId];

    return ProjectWrapper(
      initialTab: 5,
      project: project,
      child: Center(
        child: Observer(
          builder: (_) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Row(
                children: <Widget>[
                  const Text18('Project Name'),
                  const SizedBox(width: 8),
                  TextButton(
                    child: const BaText('Edit'),
                    onPressed: () {
                      Navigator.of(context).pushNamed(Routes.projectChangeName,
                          arguments: Args(project: project));
                    },
                  ),
                  const SizedBox(width: 8),
                ],
              ),
              const SizedBox(height: 4),
              Text(project.name,
                  maxLines: 3,
                  style: const TextStyle(fontSize: 18),
                  overflow: TextOverflow.ellipsis),
              const DividerOverview(),
              //

              Row(
                children: <Widget>[
                  const Text18('Notes'),
                  const SizedBox(width: 8),
                  TextButton(
                    child: const BaText('Edit'),
                    onPressed: () {
                      Navigator.of(context).pushNamed(Routes.projectChangeNotes,
                          arguments: Args(project: project));
                    },
                  ),
                ],
              ),
              Text(project.notes,
                  style: const TextStyle(fontSize: 18),
                  overflow: TextOverflow.ellipsis),
              const DividerOverview(),
            ],
          ),
        ),
      ),
    );
  }
}
