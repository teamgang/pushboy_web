import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/shared/crumbs.dart';

class ProjectWrapper extends StatefulWidget {
  const ProjectWrapper({
    required this.child,
    required this.initialTab,
    required this.project,
  });
  final Widget child;
  final int initialTab;
  final Project project;

  @override
  _ProjectWrapperState createState() => _ProjectWrapperState();
}

class _ProjectWrapperState extends State<ProjectWrapper> {
  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Crumbs(project: widget.project),
          const SizedBox(height: 30),
          Row(
            children: <Widget>[
              TextButton(
                  child: Text18('Networks',
                      style: widget.initialTab == 0
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.projectNetworksTab,
                        arguments: Args(project: widget.project));
                  }),
              const SizedBox(width: 20),
              TextButton(
                  child: Text18('Alerts',
                      style: widget.initialTab == 1
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.projectAlertsTab,
                        arguments: Args(project: widget.project));
                  }),
              const SizedBox(width: 20),
              TextButton(
                  child: Text18('Metrics',
                      style: widget.initialTab == 2
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.projectMetricsTab,
                        arguments: Args(project: widget.project));
                  }),
              //

              const SizedBox(width: 20),
              TextButton(
                  child: Text18('Plan',
                      style: widget.initialTab == 3
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.projectPlanTab,
                        arguments: Args(project: widget.project));
                  }),
              const SizedBox(width: 20),
              TextButton(
                  child: Text18('Payments',
                      style: widget.initialTab == 4
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.projectPaymentsTab,
                        arguments: Args(project: widget.project));
                  }),
              const SizedBox(width: 20),
              TextButton(
                  child: Text18('Settings',
                      style: widget.initialTab == 5
                          ? const TextStyle(fontWeight: FontWeight.bold)
                          : null),
                  onPressed: () {
                    Navigator.of(context).pushNamed(Routes.projectSettingsTab,
                        arguments: Args(project: widget.project));
                  }),
            ],
          ),
          const SizedBox(height: 20),
          Expanded(
              child: Scrollbar(
            isAlwaysShown: true,
            child: SingleChildScrollView(child: widget.child),
          )),
        ],
      ),
    );
  }
}
