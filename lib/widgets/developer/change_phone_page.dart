import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class DeveloperChangePhonePage extends StatefulWidget {
  const DeveloperChangePhonePage({required this.developer});
  final Developer developer;

  @override
  _DeveloperChangePhonePageState createState() =>
      _DeveloperChangePhonePageState();
}

class _DeveloperChangePhonePageState extends State<DeveloperChangePhonePage> {
  bool _canSave = false;
  String _currentValue = '';
  String _failureMsg = '';
  final int _maxLength = ValidateDeveloper.phoneMaxLength;
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    final developer = widget.developer;

    return WebScaffold(
      body: SizedBox.expand(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              //

              TextFormField(
                autovalidateMode: AutovalidateMode.onUserInteraction,
                enabled: !_requestInProgress,
                initialValue: developer.phone,
                keyboardType: TextInputType.multiline,
                maxLength: _maxLength,
                style: Theme.of(context).textTheme.headline6,
                decoration: InputDecoration(
                  border: const OutlineInputBorder(),
                  labelText: 'Phone number',
                  counterText: '${_currentValue.length}/$_maxLength',
                ),
                onChanged: (newValue) {
                  _currentValue = newValue;
                  if (ValidateDeveloper.phone(_currentValue).isValid) {
                    setState(() {
                      _canSave = true;
                    });
                  } else {
                    setState(() {
                      _canSave = false;
                    });
                  }
                },
                validator: (value) {
                  return ValidateDeveloper.phone(_currentValue).asValidator;
                },
              ),

              const SizedBox(height: 12),
              Text(
                  'Change your phone number on file. This number will only be used for contact in extreme circumstances.',
                  style: optionInfoStyle()),
              const SizedBox(height: 12),
              _requestInProgress
                  ? LoadingWidget()
                  : SizedBox(
                      height: 50,
                      width: 60,
                      child: ElevatedButton(
                        child: const Text18('Save'),
                        onPressed: _canSave
                            ? () {
                                FocusScope.of(context).unfocus();
                                _onSubmit(context);
                              }
                            : null,
                      ),
                    ),
              const SizedBox(height: 12),

              FailureMsg(_failureMsg),
            ],
          ),
        ),
      ),
    );
  }

  _onSubmit(BuildContext context) async {
    final developer = widget.developer;
    if (!_requestInProgress) {
      if (_currentValue == developer.phone) {
        Navigator.of(context).pop();
      }

      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.developerChangePhone,
        payload: {
          'developer_id': developer.developerId,
          'phone': _currentValue,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        developer.loadFromApi(
          context,
          developer.developerId,
        );
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
