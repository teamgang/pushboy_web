import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class DeveloperChangeEmailValidateNewLinkPage extends StatefulWidget {
  const DeveloperChangeEmailValidateNewLinkPage({required this.requestChange});
  final RequestChange requestChange;

  @override
  _DeveloperChangeEmailValidateNewLinkPageState createState() =>
      _DeveloperChangeEmailValidateNewLinkPageState();
}

class _DeveloperChangeEmailValidateNewLinkPageState
    extends State<DeveloperChangeEmailValidateNewLinkPage> {
  bool _showFailure = false;
  bool _showSuccess = false;

  @override
  initState() {
    super.initState();
    _sendConfirmation();
  }

  _sendConfirmation() async {
    ApiResponse ares = await King.of(context).lip.api(
      EndpointsV1.developerChangeEmailValidateNewLink,
      payload: {'token': widget.requestChange.token},
    );

    if (ares.isOk) {
      setState(() {
        _showSuccess = true;
      });
    } else {
      setState(() {
        _showFailure = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Column(children: <Widget>[
        _showFailure
            ? const Text18('Email change verification failed.')
            : const SizedBox.shrink(),
        _showSuccess
            ? const Text18('Email change verified. You may now log in.')
            : const SizedBox.shrink(),
      ]),
    );
  }
}
