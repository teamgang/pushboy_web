import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ConfirmEmailPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return const WebScaffold(
      body: Text22(
          'Please check your e-mail for a confirmation letter. Don\'t forget to check your spam.',
          maxLines: 4),
    );
  }
}
