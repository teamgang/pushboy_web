import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class SurferEmailConfirmLink extends StatefulWidget {
  const SurferEmailConfirmLink({required this.miscId});
  final String miscId;

  @override
  _SurferEmailConfirmLinkState createState() => _SurferEmailConfirmLinkState();
}

class _SurferEmailConfirmLinkState extends State<SurferEmailConfirmLink> {
  bool _showFailure = false;
  bool _showSuccess = false;

  @override
  initState() {
    super.initState();
    _sendConfirmation();
  }

  _sendConfirmation() async {
    ApiResponse ares = await King.of(context).lip.api(
      EndpointsV1.surferEmailVerify,
      payload: {'surfer_id': widget.miscId},
    );

    if (ares.isOk) {
      setState(() {
        _showSuccess = true;
      });
    } else {
      setState(() {
        _showFailure = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Column(children: <Widget>[
        _showFailure
            ? const Text18('Email verification failed.')
            : const SizedBox.shrink(),
        _showSuccess
            ? const Text18('Email verified. You may now log in.')
            : const SizedBox.shrink(),
      ]),
    );
  }
}
