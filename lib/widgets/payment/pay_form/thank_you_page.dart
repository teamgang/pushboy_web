import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ThankYouPage extends StatelessWidget {
  const ThankYouPage({required this.project});
  final Project project;

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Column(children: <Widget>[
        const BaText('Your payment was successsful.'),
        const SizedBox(height: 20),
        FunctionCard(
          text: 'Return to your projects',
          onTap: () {
            project.loadFromApi(context, project.projectId);
            var king = King.of(context);
            king.dad.fetchPlanChoicesWithCurrentPlan(project.planId);
            Navigator.of(context).pushNamed(Routes.projectNetworksTab,
                arguments: Args(project: project));
          },
        ),
      ]),
    );
  }
}
