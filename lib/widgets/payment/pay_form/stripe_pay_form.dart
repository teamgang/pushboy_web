import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class StripePayForm extends StatefulWidget {
  const StripePayForm({required this.paymentDetails});
  final PaymentDetails paymentDetails;

  @override
  _StripePayFormState createState() => _StripePayFormState();
}

class _StripePayFormState extends State<StripePayForm> {
  late final PaymentMethods _paymentMethods;

  @override
  initState() {
    super.initState();
    _paymentMethods = King.of(context).dad.paymentMethods;
    _paymentMethods.loadFromStripe(King.of(context));
  }

  @override
  Widget build(BuildContext context) {
    final paymentDetails = widget.paymentDetails;

    return Observer(
      builder: (_) =>
          Column(crossAxisAlignment: CrossAxisAlignment.start, children: <
              Widget>[
        const SizedBox(height: 12),
        SizedBox(
          width: 500,
          child: Observer(builder: (_) {
            var plastics = _paymentMethods.plastics.values.toList();

            return ListView.separated(
              shrinkWrap: true,
              itemCount: plastics.length,
              separatorBuilder: (context, index) => const SizedBox(height: 12),
              itemBuilder: (context, index) {
                var plastic = plastics[index];

                return Row(children: <Widget>[
                  Radio<String>(
                    value: plastic.id,
                    groupValue: widget.paymentDetails.plasticId,
                    onChanged: (String? value) {
                      widget.paymentDetails.setPlasticId(value);
                      widget.paymentDetails.isAutopaySelected = true;
                    },
                  ),
                  PlasticSlab(plastic: plastic),
                ]);
              },
            );
          }),
        ),
        //

        Row(children: <Widget>[
          Radio<String>(
            value: 'unlisted',
            groupValue: widget.paymentDetails.plasticId,
            onChanged: (String? value) {
              widget.paymentDetails.setPlasticId(value);
            },
          ),
          const TextCard('Pay with another card'),
        ]),
        const SizedBox(height: 12),

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Row(
            children: <Widget>[
              Checkbox(
                value: widget.paymentDetails.isAutopaySelected,
                onChanged: (bool? newValue) {
                  widget.paymentDetails.isAutopaySelected =
                      !widget.paymentDetails.isAutopaySelected;
                  if (widget.paymentDetails.isAutopaySelected) {
                    widget.paymentDetails.isSaveCardSelected = true;
                  }
                },
              ),
              const Expanded(
                  child: Text('Use this card for automatic payments')),
            ],
          ),
        ),
        //

        paymentDetails.isPlasticIdUnlisted
            ? Padding(
                padding:
                    const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                child: Row(
                  children: <Widget>[
                    Checkbox(
                      value: widget.paymentDetails.isSaveCardSelected,
                      onChanged: widget.paymentDetails.isAutopaySelected
                          ? null
                          : (bool? newValue) {
                              widget.paymentDetails.isSaveCardSelected =
                                  !widget.paymentDetails.isSaveCardSelected;
                              if (!widget.paymentDetails.isSaveCardSelected) {
                                widget.paymentDetails.isAutopaySelected = false;
                              }
                            },
                    ),
                    const Expanded(child: Text('Save this card')),
                  ],
                ),
              )
            : const SizedBox.shrink(),
      ]),
    );
  }
}
