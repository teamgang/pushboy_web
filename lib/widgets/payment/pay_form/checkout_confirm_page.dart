import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

const String freezePage =
    'This page has been frozen to prevent double-payments. If there was an error, please go back and return to this page.';
const String freezePageFailure =
    'Checkout failed. This page has been frozen to prevent double-payments.';

class CheckoutConfirmPage extends StatefulWidget {
  const CheckoutConfirmPage({
    required this.paymentDetails,
    required this.project,
  });
  final PaymentDetails paymentDetails;
  final Project project;

  @override
  CheckoutConfirmPageState createState() => CheckoutConfirmPageState();
}

class CheckoutConfirmPageState extends State<CheckoutConfirmPage> {
  String _failureMsg = '';
  late PaymentDetails _pd;
  late final PaymentMethods _paymentMethods;
  late final Plastic _selectedPlastic;
  bool _requestInProgress = false;

  @override
  initState() {
    super.initState();
    _paymentMethods = King.of(context).dad.paymentMethods;
    _selectedPlastic =
        _paymentMethods.plastics[widget.paymentDetails.plasticId] ?? Plastic();
    _pd = widget.paymentDetails;
  }

  List<Widget> _buildBasketItemRows() {
    List<Widget> rows = [];
    for (final item in widget.paymentDetails.basketItems) {
      rows.add(SimpleRow(
        left: item.name,
        right: '\$${item.price.uncents}',
      ));
    }
    return rows;
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Column(children: <Widget>[
        const BaText('Bill'),
        BaText(widget.project.projectId),
        const SizedBox(height: 20),
        ..._buildBasketItemRows(),

        SimpleRow(
            left: 'Applied credit:', right: '-\$${_pd.creditMax.uncents}'),
        const SizedBox(
          width: 340,
          child: Divider(color: Colors.black),
        ),
        SimpleRow(
            left: 'Total price:',
            right: '\$${_pd.totalPriceAfterCredit.uncents}'),
        const SizedBox(height: 20),
        //

        _pd.isAutopaySelected
            ? const BaText(
                'Autopay was selected. Your subscription will be automatically renewed in the future.')
            : const BaText(
                'Autopay was not selected. Please make sure you add  payment method or money to your account'),
        const SizedBox(height: 12),

        _pd.isBtcpay
            ? const BaText(
                'You chose to pay in crypto. The next screen will contain steps to finish checking out.')
            : const SizedBox.shrink(),

        _pd.isStripe
            ? const BaText('Our card payments are powered by Stripe\u2122.')
            : const SizedBox.shrink(),
        const SizedBox(height: 12),

        _pd.isPlasticIdNotEmptyNorUnlisted
            ? const BaText('You have selected to pay with')
            : const SizedBox.shrink(),
        const SizedBox(height: 12),

        _pd.isPlasticIdNotEmptyNorUnlisted
            ? SizedBox(
                width: 240,
                child: PlasticSlab(plastic: _selectedPlastic),
              )
            : const SizedBox.shrink(),

        const SizedBox(height: 12),
        FunctionCard(
          text: 'Pay now',
          onTap:
              _pd.isReadyForCheckout && !_requestInProgress && _failureMsg == ''
                  ? () {
                      _onSubmit(context);
                    }
                  : null,
        ),
        _failureMsg != '' ? Text(_failureMsg) : const SizedBox.shrink(),

        _requestInProgress ? LoadingWidget() : const SizedBox.shrink(),
      ]),
    );
  }

  Future<void> _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      final king = King.of(context);
      ApiResponse ares = await king.lip.api(
        EndpointsV1.stripePay,
        payload: _pd.asStripePayload(king),
      );

      setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        var status = readString(ares.body, 'status');
        switch (status) {
          case 'failed':
            setState(() {
              _failureMsg = freezePageFailure;
            });
            break;

          case 'success':
            setState(() {
              _failureMsg = freezePage;
            });
            if (!mounted) return;
            Navigator.of(context).pushNamed(Routes.paymentThankYou,
                arguments: Args(project: widget.project));
            break;

          case 'waitForStripe':
            setState(() {
              _failureMsg = freezePage;
            });
            var paymentAttemptId = readString(ares.body, 'payment_attempt_id');
            var stripeUrl = readString(ares.body, 'stripe_url');

            if (!mounted) return;
            Navigator.of(context).pushNamed(Routes.paymentWaitForStripe,
                arguments: Args(
                  paymentAttemptId: paymentAttemptId,
                  project: widget.project,
                  stripeUrl: stripeUrl,
                ));
            break;

          default:
            setState(() {
              _failureMsg = freezePageFailure;
            });
            king.log.wtf('invalid payment status passed from papi.');
        }
      } else {
        setState(() {
          _failureMsg = freezePageFailure;
        });
      }
    }
  }
}
