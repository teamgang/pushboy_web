import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class CheckoutCryptoPage extends StatefulWidget {
  const CheckoutCryptoPage({required this.amount, required this.project});
  final String amount;
  final Project project;

  @override
  _CheckoutCryptoPageState createState() => _CheckoutCryptoPageState();
}

class _CheckoutCryptoPageState extends State<CheckoutCryptoPage> {
  String _failureMsg = '';
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Text('You will be charged ${widget.amount}'),
      const SizedBox(height: 24),
      const Text('Which crypto type'),
      const SizedBox(height: 24),
      TextButton(
        child: const Text('Submit payment'),
        onPressed: () {
          _onSubmit(context);
        },
      ),
      _failureMsg != '' ? Text(_failureMsg) : const SizedBox.shrink(),
    ]);
  }

  Future<void> _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.paymentCheckoutCrypto,
        payload: {
          'amount': widget.amount,
          'project_id': widget.project.projectId,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        Navigator.of(context).pushReplacementNamed(Routes.paymentCryptoAddress,
            arguments: Args(amount: widget.amount));
      } else {
        this.setState(() {
          _failureMsg = 'Failed to checkout. Try again.';
        });
      }
    }
  }
}
