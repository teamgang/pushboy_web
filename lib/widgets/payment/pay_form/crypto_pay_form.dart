import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class CryptoPayForm extends StatefulWidget {
  const CryptoPayForm({required this.paymentDetails});
  final PaymentDetails paymentDetails;

  @override
  _CryptoPayFormState createState() => _CryptoPayFormState();
}

class _CryptoPayFormState extends State<CryptoPayForm> {
  @override
  Widget build(BuildContext context) {
    return Observer(
      builder: (_) => Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            const SizedBox(height: 12),
            ListView.separated(
              shrinkWrap: true,
              itemCount: Coin.values.length,
              separatorBuilder: (context, index) => const SizedBox(height: 12),
              itemBuilder: (context, index) {
                var coin = Coin.values[index];

                return Observer(
                  builder: (_) => RadioListTile<Coin>(
                    title: Text(coin.title),
                    value: coin,
                    groupValue: widget.paymentDetails.coin,
                    onChanged: (Coin? value) {
                      widget.paymentDetails.coin = value;
                    },
                  ),
                );
              },
            ),
          ]),
    );
  }
}
