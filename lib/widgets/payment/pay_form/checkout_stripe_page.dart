import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class CheckoutStripePage extends StatefulWidget {
  const CheckoutStripePage({required this.amount, required this.project});
  final String amount;
  final Project project;

  @override
  _CheckoutStripePageState createState() => _CheckoutStripePageState();
}

class _CheckoutStripePageState extends State<CheckoutStripePage> {
  String _failureMsg = '';
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      Text('You will be charged ${widget.amount}'),
      const SizedBox(height: 24),
      const Text('To card TODO'),
      const SizedBox(height: 24),
      TextButton(
        child: const Text('Submit payment'),
        onPressed: () {
          _onSubmit(context);
        },
      ),
      _failureMsg != '' ? Text(_failureMsg) : const SizedBox.shrink(),
    ]);
  }

  Future<void> _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.paymentCheckoutStripe,
        payload: {
          'amount': widget.amount,
          'project_id': widget.project.projectId,
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        Navigator.of(context).pushReplacementNamed(Routes.paymentThankYou,
            arguments: Args(amount: widget.amount));
      } else {
        this.setState(() {
          _failureMsg = 'Failed to make payment.';
        });
      }
    }
  }
}
