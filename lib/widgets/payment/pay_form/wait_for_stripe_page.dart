import 'dart:async';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

const paymentAttemptStatusSuccess = 10;
const paymentAttemptStatusWaiting = 20;
const paymentAttemptStatusFailed = 30;

class WaitForStripePage extends StatefulWidget {
  const WaitForStripePage({
    required this.paymentAttemptId,
    required this.project,
    required this.stripeUrl,
  });
  final String paymentAttemptId;
  final Project project;
  final String stripeUrl;

  @override
  WaitForStripePageState createState() => WaitForStripePageState();
}

class WaitForStripePageState extends State<WaitForStripePage> {
  String _failureMsg = '';
  late Timer _statusTimer;
  late Timer _timeoutTimer;
  bool _waiting = true;

  @override
  initState() {
    super.initState();
    _statusTimer = Timer.periodic(const Duration(seconds: 3), (Timer t) {
      checkPaymentAttemptStatus();
    });
    _timeoutTimer = Timer.periodic(const Duration(minutes: 3), (Timer t) {
      _statusTimer.cancel();
      setState(() {
        _failureMsg =
            'Payment is taking a long time to process. If your payment failed in the Stripe website, then you will have to checkout again.';
      });
    });

    launchStripeWebsite();
  }

  @override
  dispose() {
    _statusTimer.cancel();
    _timeoutTimer.cancel();
    super.dispose();
  }

  launchStripeWebsite() async {
    if (!await launchUrl(Uri.parse(widget.stripeUrl))) {
      King.of(context).snacker.addSnack(Snacks.stripeLaunchError);
    }
  }

  Future<void> checkPaymentAttemptStatus() async {
    ApiResponse ares = await King.of(context).lip.api(
      EndpointsV1.paymentAttemptStatusGetAndHandle,
      payload: {
        'payment_attempt_id': widget.paymentAttemptId,
      },
    );

    if (ares.isOk) {
      var status = readInt(ares.body, 'status');
      if (!mounted) return;
      switch (status) {
        case paymentAttemptStatusSuccess:
          _statusTimer.cancel();
          setState(() {
            _failureMsg = '';
            _waiting = false;
          });
          Navigator.of(context).pushReplacementNamed(Routes.paymentThankYou,
              arguments: Args(project: widget.project));
          break;

        case paymentAttemptStatusWaiting:
          // keep waiting
          break;

        case paymentAttemptStatusFailed:
          _statusTimer.cancel();
          setState(() {
            _failureMsg = 'Payment failed.';
            _waiting = false;
          });
          Navigator.of(context).pushReplacementNamed(Routes.paymentFailed,
              arguments: Args(project: widget.project));
          break;

        default:
          _statusTimer.cancel();
          King.of(context).log.i('payment failed ${widget.paymentAttemptId}');
          Navigator.of(context).pushReplacementNamed(Routes.paymentFailed,
              arguments: Args(project: widget.project));
          King.of(context).snacker.addSnack(Snacks.paymentError);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Column(children: <Widget>[
        const SizedBox(height: 20),
        const BaText('Your payment must be completed in an external window.'),
        _waiting ? LoadingWidget() : const SizedBox.shrink(),
        const SizedBox(height: 20),
        _failureMsg != '' ? BaText(_failureMsg) : const SizedBox.shrink(),
        const SizedBox(height: 20),
      ]),
    );
  }
}
