import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/utils/select_period_data.dart';
import 'package:pushboy_web/widgets/shared/select_period.dart';

class BillingActivityPage extends StatefulWidget {
  const BillingActivityPage({required this.project});
  final Project project;

  @override
  _BillingActivityPageState createState() => _BillingActivityPageState();
}

class _BillingActivityPageState extends State<BillingActivityPage> {
  bool showAllMethods = false;
  final SelectPeriodData data = SelectPeriodData();

  @override
  initState() {
    super.initState();
    data.project = widget.project;
  }

  @override
  Widget build(BuildContext context) {
    final project = widget.project;

    return WebScaffold(
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                const SizedBox(height: 24),
                Observer(
                  builder: (_) => Text18('Project: ${project.name}'),
                ),
                const SizedBox(height: 24),
                //

                const Text18('Select a billing period'),
                const SizedBox(height: 24),

                SelectPeriod(data: data),
                const SizedBox(height: 24),

                Observer(
                  builder: (_) => data.isSelectedDateValid
                      ? Text18(
                          'Showing the billing activity for the period of ${data.printSelectedDate}.')
                      : const SizedBox.shrink(),
                ),
                const SizedBox(height: 24),

                Observer(
                  builder: (_) => data.isSelectedDateValid
                      ? ActivitySummary(
                          month: data.selectedMonth,
                          project: project,
                          year: data.selectedYear,
                        )
                      : const SizedBox.shrink(),
                ),
                const SizedBox(height: 24),

                //FunctionCard(
                //text: 'Download as a .csv document (in progress)',
                //onTap: () {},
                //),
                //
              ]),
        ),
      ),
    );
  }
}

class ActivitySummary extends StatefulWidget {
  const ActivitySummary({
    required this.month,
    required this.year,
    required this.project,
  });
  final int month;
  final Project project;
  final int year;

  @override
  _ActivitySummaryState createState() => _ActivitySummaryState();
}

class _ActivitySummaryState extends State<ActivitySummary> {
  @override
  Widget build(BuildContext context) {
    final project = widget.project;
    final bois = King.of(context).dad.bois.getBoisOfProjectAndPeriod(
          projectId: widget.project.projectId,
          year: widget.year,
          month: widget.month,
        );

    final calc = King.of(context).dad.bois.latestCalc;
    calc.plan = King.of(context).dad.getPlan(project.planId);

    return Observer(
      builder: (_) {
        project.loadStaticEditTokenInfo(context);

        return Column(
          children: <Widget>[
            const SizedBox(height: 24),
            SimpleRow(left: 'Static Sent', right: '${calc.staticSent}'),
            SimpleRow(
                left: 'Static Free', right: '${calc.plan.monthlyFreeStatic}'),
            SimpleRow(
                left: 'Static Packages to Purchase',
                right: '${calc.staticPurchases}'),
            SimpleRow(
                left: 'Total Static Price', right: calc.staticTotalPrice.toUsd),
            const SizedBox(height: 24),
            //

            SimpleRow(left: 'Custom Sent', right: '${calc.customSent}'),
            SimpleRow(
                left: 'Custom Free', right: '${calc.plan.monthlyFreeCustom}'),
            SimpleRow(
                left: 'Custom Packages to Purchase',
                right: '${calc.customPurchases}'),
            SimpleRow(
                left: 'Total Custom Price', right: calc.customTotalPrice.toUsd),
            const SizedBox(height: 24),
            //

            SimpleRow(left: 'Limitless Sent', right: '${calc.limitlessSent}'),
            SimpleRow(
                left: 'Limitless Free',
                right: '${calc.plan.monthlyFreeLimitless}'),
            SimpleRow(
                left: 'Limitless Packages to Purchase',
                right: '${calc.limitlessPurchases}'),
            SimpleRow(
                left: 'Total Limitless Price',
                right: calc.limitlessTotalPrice.toUsd),
            const SizedBox(height: 24),
            //

            SimpleRow(
                left: 'Edit Static Tokens Purchased',
                right: '${project.statEditTokensBoughtTotal}'),
            SimpleRow(
                left: 'Total Edit Static Tokens Price',
                right: project.statEditTokensBoughtPrice(calc.plan).toUsd),
            const SizedBox(height: 24),
            //

            SimpleRow(left: 'Total Price', right: calc.totalPrice.toUsd),
            const SizedBox(height: 24),

            ListView.separated(
              shrinkWrap: true,
              reverse: true,
              itemCount: bois.length,
              separatorBuilder: (context, index) => const SizedBox(height: 8),
              itemBuilder: (context, index) {
                var boi = bois[index];
                return BoiSlab(
                    index: index,
                    boi: boi,
                    onTap: () {
                      Navigator.of(context).pushNamed(Routes.boiStatus,
                          arguments: Args(boi: boi));
                    });
              },
            ),
          ],
        );
      },
    );
  }
}
