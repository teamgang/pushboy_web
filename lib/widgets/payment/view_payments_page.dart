import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ViewPaymentsPage extends StatefulWidget {
  const ViewPaymentsPage({required this.project});
  final Project project;

  @override
  _ViewPaymentsPageState createState() => _ViewPaymentsPageState();
}

class _ViewPaymentsPageState extends State<ViewPaymentsPage> {
  PaymentAttemptType _selected = PaymentAttemptType.successful;

  @override
  initState() {
    super.initState();
    King.of(context).dad.loadPaymentsOfProject(
          widget.project.projectId,
          _selected,
        );
  }

  bool get _isShowCcAttempts {
    return _selected == PaymentAttemptType.ccAttempt ? true : false;
  }

  bool get _isShowSuccessful {
    return _selected == PaymentAttemptType.successful ? true : false;
  }

  _setShowSuccessful() {
    setState(() {
      _selected = PaymentAttemptType.successful;
      King.of(context).dad.loadPaymentsOfProject(
            widget.project.projectId,
            _selected,
          );
    });
  }

  _setShowCcAttempts() {
    setState(() {
      _selected = PaymentAttemptType.ccAttempt;
      King.of(context).dad.loadPaymentsOfProject(
            widget.project.projectId,
            _selected,
          );
    });
  }

  @override
  Widget build(BuildContext context) {
    final project = widget.project;
    final payments = King.of(context).dad.payments;

    return WebScaffold(
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const SizedBox(height: 24),
            Observer(
              builder: (_) => Text18('Payments for Project: ${project.name}'),
            ),
            const SizedBox(height: 24),
            //

            const Text18('Filter:'),
            const SizedBox(height: 24),

            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: <Widget>[
                  Checkbox(
                    value: _isShowSuccessful,
                    onChanged: (bool? newValue) => _setShowSuccessful(),
                  ),
                  const Expanded(child: Text('Successful Payments')),
                ],
              ),
            ),
            const SizedBox(width: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              child: Row(
                children: <Widget>[
                  Checkbox(
                    value: _isShowCcAttempts,
                    onChanged: (bool? newValue) => _setShowCcAttempts(),
                  ),
                  const Expanded(child: Text('All Payment Attempts')),
                ],
              ),
            ),
            const SizedBox(height: 24),

            Observer(builder: (_) {
              return ListView.separated(
                shrinkWrap: true,
                itemCount: payments.length,
                separatorBuilder: (context, index) => const SizedBox(height: 8),
                itemBuilder: (context, index) {
                  var payment = payments[index];
                  return PaymentCard(
                    index: index,
                    payment: payment,
                  );
                },
              );
              //
            }),
            const SizedBox(height: 24),
          ]),
    );
  }
}

class PaymentCard extends StatelessWidget {
  const PaymentCard({
    required this.index,
    required this.payment,
  });
  final int index;
  final Payment payment;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text18('${index + 1}. Payment for ${payment.silo.human}'),
              const SizedBox(height: 4),
              Text18('Price: ${payment.totalPrice.toUsd}'),
              const SizedBox(height: 4),

              Text18('Credit Applied: ${payment.creditApplied.toUsd}'),
              const SizedBox(height: 4),
              //

              payment.silo == PaymentSilo.billPay
                  ? FunctionCard(
                      text: 'View Bill',
                      onTap: () async {
                        var bill = Bill();
                        await bill.loadFromApi(context, payment.billId);
                        Navigator.of(context).pushNamed(Routes.billOverview,
                            arguments: Args(bill: bill));
                      },
                    )
                  : const SizedBox.shrink(),

              SelectableText('Payment ID: ${payment.paymentId}'),
              const SizedBox(height: 4),

              SelectableText('Attempt ID: ${payment.attemptId}'),
              const SizedBox(height: 4),

              SelectableText('Bill ID: ${payment.billId}'),
              const SizedBox(height: 4),

              SelectableText('Status: ${payment.status.human}'),
              const SizedBox(height: 4),
            ]),
      ),
    );
  }
}
