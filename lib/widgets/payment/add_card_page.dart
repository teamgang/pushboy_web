import 'package:credit_card_validator/credit_card_validator.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class AddCardPage extends StatefulWidget {
  const AddCardPage({required this.project});
  final Project project;

  @override
  AddCardPageState createState() => AddCardPageState();
}

class AddCardPageState extends State<AddCardPage> {
  bool _canSave = false;
  String _cvv = '';
  String _mm = '';
  String _dd = '';
  late FocusNode _mmFocusNode;
  late FocusNode _ddFocusNode;
  late FocusNode _cvvFocusNode;
  final TextEditingController _mmController = TextEditingController(text: '');
  final TextEditingController _ddController = TextEditingController(text: '');
  final TextEditingController _cvvController = TextEditingController(text: '');
  String _number = '';
  String _failureMsg = '';
  final CreditCardValidator _ccValidator = CreditCardValidator();
  bool _requestInProgress = false;

  @override
  initState() {
    super.initState();
    _mmFocusNode = FocusNode();
    _ddFocusNode = FocusNode();
    _cvvFocusNode = FocusNode();
  }

  @override
  dispose() {
    _mmFocusNode.dispose();
    _ddFocusNode.dispose();
    _cvvFocusNode.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: FractionallySizedBox(
        widthFactor: 1,
        child: Center(
          child: SizedBox(
            width: 260,
            child: Column(children: <Widget>[
              TextFormField(
                autofocus: true,
                key: const Key(XKeys.ccFormNumberField),
                decoration: const InputDecoration(
                  labelText: 'Card Number',
                ),
                maxLength: 19,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                autocorrect: false,
                keyboardType: TextInputType.number,
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                ],
                textInputAction: TextInputAction.next,
                onChanged: (value) {
                  _number = value.trim();
                },
                validator: (value) {
                  var results = _ccValidator.validateCCNum(value ?? '');
                  return results.message;
                },
              ),
              //

              Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    SizedBox(
                      width: 80,
                      child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            SizedBox(
                              width: 30,
                              child: RawKeyboardListener(
                                focusNode: FocusNode(), // or FocusNode()
                                onKey: (event) {
                                  if (_mmController.text.length == 2) {
                                    _ddFocusNode.requestFocus();
                                  }
                                },
                                child: TextFormField(
                                  controller: _mmController,
                                  key: const Key(XKeys.ccFormExpirationFieldMm),
                                  decoration: const InputDecoration(
                                    labelText: 'MM',
                                  ),
                                  maxLength: 2,
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  autocorrect: false,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(
                                        RegExp('[0-9]')),
                                  ],
                                  textInputAction: TextInputAction.next,
                                  focusNode: _mmFocusNode,
                                  onChanged: (value) {
                                    _mm = value;
                                  },
                                  validator: (value) {
                                    var results = _ccValidator
                                        .validateExpDate(value ?? '');
                                    _failureMsg = results.message;
                                    return null;
                                  },
                                ),
                              ),
                            ),
                            const Text('/'),
                            //

                            SizedBox(
                              width: 30,
                              child: RawKeyboardListener(
                                focusNode: FocusNode(), // or FocusNode()
                                onKey: (event) {
                                  if (event.logicalKey ==
                                      LogicalKeyboardKey.backspace) {
                                    if (_ddController.text == '') {
                                      _mmFocusNode.requestFocus();
                                    }
                                  }
                                  if (_ddController.text.length == 2) {
                                    _cvvFocusNode.requestFocus();
                                  }
                                },
                                child: TextFormField(
                                  controller: _ddController,
                                  key: const Key(XKeys.ccFormExpirationFieldDd),
                                  decoration: const InputDecoration(
                                    labelText: 'DD',
                                  ),
                                  maxLength: 2,
                                  autovalidateMode:
                                      AutovalidateMode.onUserInteraction,
                                  autocorrect: false,
                                  keyboardType: TextInputType.number,
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(
                                        RegExp('[0-9]')),
                                  ],
                                  textInputAction: TextInputAction.next,
                                  focusNode: _ddFocusNode,
                                  onChanged: (value) {
                                    _dd = value;
                                  },
                                  validator: (value) {
                                    _failureMsg = _validateExpiration(_mm, _dd);
                                    return null;
                                  },
                                ),
                              ),
                            ),
                          ]),
                    ),
                    //

                    SizedBox(
                      width: 80,
                      child: RawKeyboardListener(
                        focusNode: FocusNode(), // or FocusNode()
                        onKey: (event) {
                          if (event.logicalKey ==
                              LogicalKeyboardKey.backspace) {
                            if (_cvvController.text == '') {
                              _ddFocusNode.requestFocus();
                            }
                          }
                        },
                        child: TextFormField(
                          controller: _cvvController,
                          obscureText: true,
                          key: const Key(XKeys.ccFormCvvField),
                          decoration: const InputDecoration(
                            labelText: 'CVV',
                          ),
                          maxLength: 4,
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          autocorrect: false,
                          keyboardType: TextInputType.number,
                          inputFormatters: [
                            FilteringTextInputFormatter.allow(RegExp('[0-9]')),
                          ],
                          textInputAction: TextInputAction.done,
                          focusNode: _cvvFocusNode,
                          onChanged: (value) {
                            _cvv = value;
                          },
                          validator: (value) {
                            var numberResults =
                                _ccValidator.validateCCNum(_number);
                            var results = _ccValidator.validateCVV(
                                _cvv, numberResults.ccType);
                            _failureMsg = results.message;
                            return null;
                          },
                        ),
                      ),
                    ),
                  ]),
              //

              _failureMsg != '' ? Text(_failureMsg) : const SizedBox.shrink(),
              IconButton(
                icon: Icon(Icons.check,
                    color: _canSave ? Colors.green : Colors.black),
                onPressed: () {
                  FocusScope.of(context).unfocus();
                  _onSubmit(context);
                },
              ),
            ]),
          ),
        ),
      ),
    );
  }

  String _validateExpiration(String mm, String dd) {
    return _ccValidator.validateExpDate('$mm/$dd').message;
  }

  Future<void> _onSubmit(BuildContext context) async {
    if (!_requestInProgress) {
      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        //EndpointsV1.stripeSaveCreditCard,
        'might not use this page',
        payload: {},
      );

      this.setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        //King.of(context).dad.;
        Navigator.of(context).pop();
      } else {
        this.setState(() {
          _failureMsg = 'Failed to submit change.';
        });
      }
    }
  }
}
