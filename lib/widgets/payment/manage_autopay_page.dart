import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ManageAutopayPage extends StatefulWidget {
  const ManageAutopayPage({required this.project});
  final Project project;

  @override
  ManageAutopayPageState createState() => ManageAutopayPageState();
}

class ManageAutopayPageState extends State<ManageAutopayPage> {
  String _failureMsg = '';
  bool _isAgreementAccepted = false;
  bool _requestInProgress = false;
  String _selectedPlasticId = '';

  @override
  initState() {
    super.initState();
    King.of(context).dad.getPaymentMethods();
    King.of(context).dad.legal.loadFromApi(context);
  }

  bool get _canSave {
    return _selectedPlasticId.isNotEmpty &&
        _isAgreementAccepted &&
        _selectedPlasticId != widget.project.defaultPlasticId;
  }

  @override
  Widget build(BuildContext context) {
    final project = widget.project;
    final paymentMethods = King.of(context).dad.paymentMethods;

    return WebScaffold(
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const SizedBox(height: 24),
            Observer(
              builder: (_) => Text20('Autopay for ${widget.project.name}'),
            ),
            const SizedBox(height: 24),
            const Text18('Current card used for autopayment:'),
            const SizedBox(height: 24),

            Observer(
              builder: (_) => project.defaultPlasticId.isEmpty
                  ? const Text18('No card selected for autopay.')
                  : SizedBox(
                      width: 500,
                      child: Observer(builder: (_) {
                        return PlasticSlab(
                            plastic: paymentMethods
                                .getPlastic(project.defaultPlasticId));
                      }),
                    ),
            ),
            const SizedBox(height: 24),

            const Text18('Select a card below to change your autopay method.'),
            const SizedBox(height: 20),

            FunctionCard(
              text: 'Add a card',
              onTap: () {
                _onAddCard(context);
              },
            ),
            const SizedBox(height: 20),
            //

            _step2(),
          ]),
    );
  }

  Widget _step2() {
    final project = widget.project;
    final paymentMethods = King.of(context).dad.paymentMethods;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(
          width: 500,
          child: Observer(builder: (_) {
            final plastics = paymentMethods.plastics.values.toList();

            return ListView.separated(
              shrinkWrap: true,
              itemCount: plastics.length,
              separatorBuilder: (context, index) => const SizedBox(height: 12),
              itemBuilder: (context, index) {
                var plastic = plastics[index];

                return Row(children: <Widget>[
                  Radio<String>(
                    value: plastic.id,
                    groupValue: _selectedPlasticId,
                    onChanged: (String? value) {
                      setState(() {
                        _selectedPlasticId = plastic.id;
                      });
                    },
                  ),
                  PlasticSlab(plastic: plastic),
                ]);
              },
            );
          }),
        ),
        const SizedBox(height: 12),
        //

        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Row(
            children: <Widget>[
              Checkbox(
                value: _isAgreementAccepted,
                onChanged: (bool? newValue) {
                  setState(() {
                    _isAgreementAccepted = !_isAgreementAccepted;
                  });
                },
              ),
              const Expanded(child: AcceptTermsButton()),
            ],
          ),
        ),
        const SizedBox(height: 12),

        FunctionCard(
          text: 'Use this card for autopay',
          onTap: _canSave
              ? () {
                  _onSubmit(context);
                }
              : null,
        ),
        const SizedBox(height: 12),

        FunctionCard(
          text: 'Delete this card',
          onTap: _selectedPlasticId.isNotEmpty
              ? () {
                  _showDeleteCardModal(context);
                }
              : null,
        ),
        const SizedBox(height: 12),

        Observer(
          builder: (_) => Container(
            color: project.isAutopayEnabled ? Colors.green : Colors.red,
            child: SizedBox(
              width: 400,
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: project.isAutopayEnabled
                    ? Row(children: <Widget>[
                        const Text18('Autopay is enabled'),
                        const SizedBox(width: 20),
                        FunctionCard(
                          text: 'Disable autopay',
                          onTap: () {
                            _submitChangeAutopay(context, false);
                          },
                        ),
                      ])
                    : Row(children: <Widget>[
                        const Text18('Autopay is disabled'),
                        const SizedBox(width: 20),
                        FunctionCard(
                          text: 'Enable autopay',
                          onTap: () {
                            _submitChangeAutopay(context, true);
                          },
                        ),
                      ]),
              ),
            ),
          ),
        ),
        const SizedBox(height: 12),

        _failureMsg.isEmpty
            ? const SizedBox.shrink()
            : Text(_failureMsg, style: Bast.failure),
      ],
    );
  }

  void _showDeleteCardModal(BuildContext context) {
    showModalBottomSheet<void>(
      context: context,
      builder: (BuildContext context) {
        return Container(
          height: 200,
          width: 400,
          color: Colors.amber,
          child: Center(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  const Text('Are you sure you want to delete this card?'),
                  const SizedBox(height: 12),
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        ElevatedButton(
                            child: const Text('Yes'),
                            onPressed: () async {
                              await _onDeleteCard(context);
                              if (!mounted) return;
                              Navigator.of(context).pop();
                            }),
                        const SizedBox(width: 60),
                        ElevatedButton(
                          child: const Text('No'),
                          onPressed: () => Navigator.pop(context),
                        ),
                      ]),
                ]),
          ),
        );
      },
    );
  }

  _onAddCard(BuildContext context) async {
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.projectPlasticAdd,
        payload: {},
      );

      setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        String stripeUrl = ares.body['stripe_url'];
        if (!mounted) return;
        if (!await launchUrl(Uri.parse(stripeUrl))) {
          King.of(context).snacker.addSnack(Snacks.stripeLaunchError);
        } else {
          Navigator.of(context).pop();
        }
      } else {
        setState(() {
          _failureMsg =
              ares.peekErrorMsg(defaultText: 'Failed to submit change.');
        });
      }
    }
  }

  _onDeleteCard(BuildContext context) async {
    final project = widget.project;
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      final king = King.of(context);
      final ares = await king.dad.paymentMethods
          .deleteOnStripe(king, _selectedPlasticId);

      ApiResponse ares2 = await king.lip.api(
        EndpointsV1.projectPlasticDelete,
        payload: {
          'plastic_id': _selectedPlasticId,
          'project_id': project.projectId,
        },
      );

      setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk && ares2.isOk) {
        king.dad.paymentMethods.plastics.clear();
        if (!mounted) return;
        project.loadFromApi(context, project.projectId);
        king.dad.getPaymentMethods();
      } else {
        setState(() {
          _failureMsg =
              ares.peekErrorMsg(defaultText: 'Failed to submit change.');
        });
      }
    }
  }

  _onSubmit(BuildContext context) async {
    final king = King.of(context);
    final project = widget.project;
    if (!_requestInProgress) {
      if (_selectedPlasticId == project.defaultPlasticId) {
        Navigator.of(context).pop();
      }

      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await king.lip.api(
        EndpointsV1.projectChangeDefaultPlasticId,
        payload: {
          'default_plastic_id': _selectedPlasticId,
          'project_id': project.projectId,
          'terms_accepted_version':
              _isAgreementAccepted ? king.dad.legal.developerTermsVersion : '',
        },
      );

      setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        if (!mounted) return;
        project.loadFromApi(
          context,
          project.projectId,
        );
        Navigator.of(context).pop();
      } else {
        setState(() {
          _failureMsg =
              ares.peekErrorMsg(defaultText: 'Failed to submit change.');
        });
      }
    }
  }

  _submitChangeAutopay(BuildContext context, bool enableAutopay) async {
    if (!_requestInProgress) {
      setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await King.of(context).lip.api(
        EndpointsV1.projectChangeAutopay,
        payload: {
          'enable_autopay': enableAutopay,
          'project_id': widget.project.projectId,
        },
      );

      setState(() {
        _requestInProgress = false;
      });
      if (ares.isOk) {
        if (!mounted) return;
        widget.project.loadFromApi(
          context,
          widget.project.projectId,
        );
      } else {
        setState(() {
          _failureMsg =
              ares.peekErrorMsg(defaultText: 'Failed to submit change.');
        });
      }
    }
  }
}
