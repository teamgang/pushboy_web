import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ViewBillsPage extends StatefulWidget {
  const ViewBillsPage({required this.project});
  final Project project;

  @override
  _ViewBillsPageState createState() => _ViewBillsPageState();
}

class _ViewBillsPageState extends State<ViewBillsPage> {
  @override
  initState() {
    super.initState();
    King.of(context).dad.loadBillsOfProject(widget.project.projectId);
  }

  @override
  Widget build(BuildContext context) {
    final project = widget.project;
    final bills = King.of(context).dad.bills;

    return WebScaffold(
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            const SizedBox(height: 24),
            Observer(
              builder: (_) => Text18('Project: ${project.name}'),
            ),
            const SizedBox(height: 24),
            //

            const Text18('Billing Periods:'),
            const SizedBox(height: 24),

            Observer(builder: (_) {
              return ListView.separated(
                shrinkWrap: true,
                itemCount: bills.length,
                separatorBuilder: (context, index) => const SizedBox(height: 8),
                itemBuilder: (context, index) {
                  var bill = bills[index];
                  return FunctionCard(
                      text:
                          'Bill for ${periodToHuman(bill.period)}. Status: ${bill.status}',
                      onTap: () {
                        Navigator.of(context).pushNamed(Routes.billOverview,
                            arguments:
                                Args(bill: bill, project: widget.project));
                      });
                },
              );
            }),
            const SizedBox(height: 24),
          ]),
    );
  }
}
