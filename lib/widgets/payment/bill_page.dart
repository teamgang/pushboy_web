import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class BillPage extends StatefulWidget {
  const BillPage({required this.bill, required this.project});
  final Bill bill;
  final Project project;

  @override
  _BillPageState createState() => _BillPageState();
}

class _BillPageState extends State<BillPage> {
  @override
  initState() {
    super.initState();
    King.of(context).dad.loadChargesOfBill(widget.bill.billId);
  }

  @override
  Widget build(BuildContext context) {
    final bill = widget.bill;
    final project = widget.project;
    final charges = King.of(context).dad.charges;

    return WebScaffold(
      body: Scrollbar(
        child: SingleChildScrollView(
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Observer(builder: (_) {
                  return Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text18('Project: ${project.name}'),
                        const SizedBox(height: 8),

                        Text18('Bill for period ${periodToHuman(bill.period)}'),
                        const SizedBox(height: 12),

                        SimpleRow(left: 'Total Price', right: bill.price.toUsd),
                        const SizedBox(height: 8),

                        SimpleRow(left: 'Total Price', right: bill.price.toUsd),
                        const SizedBox(height: 8),
                        //

                        SimpleRow(
                            left: 'Credit Applied',
                            right: bill.creditApplied.toUsd),
                        const SizedBox(height: 8),

                        SimpleRow(left: 'Status', right: bill.status),
                        const SizedBox(height: 16),

                        SimpleRow(
                            left: 'Is autopayment enabled?',
                            right: project.isAutopayEnabled ? 'Yes' : 'No'),
                        const SizedBox(height: 12),

                        const Text18(
                            'If autopayment is enabled or if there is enough credit on your account, then your bill will be paid automatically on the 7th of each month. If payment fails, you can pay your bill here.'),

                        const SizedBox(height: 12),
                        bill.needsPayment
                            ? FunctionCard(
                                text: 'Pay now',
                                onTap: () {
                                  Navigator.of(context).pushNamed(
                                      Routes.billPayNow,
                                      arguments:
                                          Args(bill: bill, project: project));
                                })
                            : const SizedBox.shrink(),
                        const SizedBox(height: 16),

                        const Text18('Charges:'),
                      ]);
                }),
                Observer(
                  builder: (_) => ListView.separated(
                    shrinkWrap: true,
                    itemCount: charges.length,
                    separatorBuilder: (context, index) =>
                        const SizedBox(height: 8),
                    itemBuilder: (context, index) {
                      var charge = charges[index];

                      return ChargeCard(
                        charge: charge,
                        index: index,
                        project: project,
                      );
                    },
                  ),
                ),
                const SizedBox(height: 24),
              ]),
        ),
      ),
    );
  }
}

class ChargeCard extends StatelessWidget {
  const ChargeCard({
    required this.charge,
    required this.index,
    required this.project,
  });
  final int index;
  final Charge charge;
  final Project project;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
        onTap: () {
          if (charge.boiId.isNotEmpty) {
            final boi = Boi();
            boi.loadFromApi(context, charge.boiId);

            Navigator.of(context)
                .pushNamed(Routes.boiStatus, arguments: Args(boi: boi));
          }
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
          child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text18('${index + 1}. ${charge.title}'),
                const SizedBox(height: 4),
                Text18('Price: ${charge.price.toUsd}'),
                const SizedBox(height: 4),
                SelectableText('Charge ID: ${charge.chargeId}'),
                const SizedBox(height: 4),
                charge.isBoiCharge
                    ? SelectableText('Notification ID: ${charge.boiId}')
                    : const SizedBox.shrink(),
              ]),
        ),
      ),
    );
  }
}
