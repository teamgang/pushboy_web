import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/payment/pay_form/pay_form.dart';

class PlanChangePage extends StatefulWidget {
  const PlanChangePage({required this.plan, required this.project});
  final Plan plan;
  final Project project;

  @override
  _PlanChangePageState createState() => _PlanChangePageState();
}

class _PlanChangePageState extends State<PlanChangePage> {
  final _currentPlan = Plan();
  PaymentDetails paymentDetails = PaymentDetails();
  final BasketItem planBasketItem = BasketItem();

  @override
  initState() {
    super.initState();
    var king = King.of(context);
    _currentPlan.loadFromApi(king, widget.project.planId);
    planBasketItem.itemType = BasketItemType.plan;
    planBasketItem.name = '${widget.plan.title} plan subscription';
    planBasketItem.planId = widget.plan.planId;
    planBasketItem.price = widget.plan.price;
    planBasketItem.projectId = widget.project.projectId;
    paymentDetails.addToBasket(planBasketItem);
    paymentDetails.applyCredit = widget.project.credit;
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Center(
        child: Observer(
          builder: (_) => Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              TextCard(
                  'You current plan is ${_currentPlan.title}. You are attempting to change your plan to ${widget.plan.title}.'),
              //
              TextCard(widget.plan.priceForDisplaySentence),
              const TextCard(
                  'You will be charged for additional notifications and other services on the 1st of each month. If you miss a payment, your plan will continue to operate for 14 days before your service will be disabled.'),
              TextCard(
                  'Any credit on your project will be applied now. After this transaction, your project will have \$${paymentDetails.creditAfterTransaction.uncents} in credit.'),
              const BaText('Cost Summary'),
              const SizedBox(height: 20),

              SimpleRow(
                  left: 'Price:',
                  right: '\$${paymentDetails.totalPrice.uncents}'),
              SimpleRow(
                  left: 'Applied credit:',
                  right: '\$${paymentDetails.creditMax.uncents}'),
              const SizedBox(
                width: 340,
                child: Divider(color: Colors.black),
              ),
              SimpleRow(
                  left: 'Amount due now:',
                  right: '\$${paymentDetails.totalPriceAfterCredit.uncents}'),
              //

              const SizedBox(height: 20),

              widget.project.mustChangePlanNow
                  ? PayForm(
                      paymentDetails: paymentDetails,
                      project: widget.project,
                    )
                  : PayFormOptionContainer(
                      paymentDetails: paymentDetails,
                      plan: widget.plan,
                      project: widget.project,
                    ),
            ],
          ),
        ),
        //
      ),
    );
  }
}

class PayFormOptionContainer extends StatefulWidget {
  const PayFormOptionContainer({
    required this.paymentDetails,
    required this.plan,
    required this.project,
  });
  final Project project;
  final Plan plan;
  final PaymentDetails paymentDetails;

  @override
  _PayFormOptionContainerState createState() => _PayFormOptionContainerState();
}

class _PayFormOptionContainerState extends State<PayFormOptionContainer> {
  bool _isAgreementAccepted = false;
  String _failureMsg = '';
  int _option = 0;
  bool _requestInProgress = false;

  @override
  Widget build(BuildContext context) {
    return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          const TextCard('Since you already have a plan, you have 2 choices.'),
          const TextCard(
              '1) You can change your plan immediately, and access the changes right now (better choice if you want to upgrade), or;'),
          const TextCard(
              '2) You may change your plan at the end of your current billing cycle (better choice if you want to cancel).'),
          FunctionCard(
              text: 'Change plan now (Option 1)',
              onTap: _option == 1
                  ? null
                  : () {
                      setState(() {
                        _option = 1;
                      });
                    }),
          FunctionCard(
              text: 'Change plan at the start of next cycle (Option 2)',
              onTap: _option == 2
                  ? null
                  : () {
                      setState(() {
                        _option = 2;
                      });
                    }),
          _option == 1
              ? PayForm(
                  paymentDetails: widget.paymentDetails,
                  project: widget.project,
                )
              : const SizedBox.shrink(),
          //

          _option == 2
              ? Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.symmetric(
                        horizontal: 10, vertical: 10),
                    child: Row(
                      children: <Widget>[
                        Checkbox(
                          value: _isAgreementAccepted,
                          onChanged: (bool? newValue) {
                            setState(() {
                              _isAgreementAccepted = !_isAgreementAccepted;
                            });
                          },
                        ),
                        const Expanded(child: AcceptTermsButton()),
                      ],
                    ),
                  ),
                  FunctionCard(
                      text: 'Submit plan change',
                      onTap: !_isAgreementAccepted
                          ? null
                          : () {
                              _onSubmit(context);
                            }),
                ])
              : const SizedBox.shrink(),
          _failureMsg != '' ? Text(_failureMsg) : const SizedBox.shrink(),
        ]);
  }

  Future<void> _onSubmit(BuildContext context) async {
    final king = King.of(context);
    if (!_requestInProgress) {
      this.setState(() {
        _failureMsg = '';
        _requestInProgress = true;
      });

      ApiResponse ares = await king.lip.api(
        EndpointsV1.projectPlanIdNextChange,
        payload: {
          'project_id': widget.project.projectId,
          'plan_id_next': widget.plan.planId,
          'terms_accepted_version':
              _isAgreementAccepted ? king.dad.legal.developerTermsVersion : '',
        },
      );

      this.setState(() {
        _requestInProgress = false;
      });

      if (ares.isOk) {
        Navigator.of(context).pushNamed(Routes.home);
      } else {
        this.setState(() {
          _failureMsg = 'Failed to set plan change.';
        });
      }
    }
  }
}
