import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';
import 'package:pushboy_web/widgets/alerts_page.dart';
import 'package:pushboy_web/widgets/contact_page.dart';
import 'package:pushboy_web/widgets/home_page.dart';

import 'package:pushboy_web/widgets/boi/change_channel_page.dart';
import 'package:pushboy_web/widgets/boi/layout_descriptions_page.dart';
import 'package:pushboy_web/widgets/boi/send_page.dart';
import 'package:pushboy_web/widgets/boi/status_page.dart';

import 'package:pushboy_web/widgets/channel/change_abbrev_page.dart';
import 'package:pushboy_web/widgets/channel/change_color_page.dart';
import 'package:pushboy_web/widgets/channel/change_description_page.dart';
import 'package:pushboy_web/widgets/channel/change_name_page.dart';
import 'package:pushboy_web/widgets/channel/change_notes_page.dart';
import 'package:pushboy_web/widgets/channel/change_route_page.dart';
import 'package:pushboy_web/widgets/channel/channel_create_page.dart';
import 'package:pushboy_web/widgets/channel/channel_tokens_overview_page.dart';
import 'package:pushboy_web/widgets/channel/edit_page.dart';
import 'package:pushboy_web/widgets/channel/overview_page.dart';

import 'package:pushboy_web/widgets/console/console_page.dart';
import 'package:pushboy_web/widgets/developer/developer_page.dart';
import 'package:pushboy_web/widgets/developer/change_email_request.dart';
import 'package:pushboy_web/widgets/developer/change_email_link.dart';
import 'package:pushboy_web/widgets/developer/change_email_validate_new_link.dart';
import 'package:pushboy_web/widgets/developer/change_password_request.dart';
import 'package:pushboy_web/widgets/developer/change_password_link.dart';
import 'package:pushboy_web/widgets/developer/change_phone_page.dart';
import 'package:pushboy_web/widgets/developer/logged_out_change_password_request.dart';

import 'package:pushboy_web/widgets/surfer/change_email_link.dart';
import 'package:pushboy_web/widgets/surfer/change_email_validate_new_link.dart';
import 'package:pushboy_web/widgets/surfer/change_password_link.dart';

import 'package:pushboy_web/widgets/network/change_abbrev_page.dart';
import 'package:pushboy_web/widgets/network/change_color_page.dart';
import 'package:pushboy_web/widgets/network/change_description_page.dart';
import 'package:pushboy_web/widgets/network/change_name_page.dart';
import 'package:pushboy_web/widgets/network/change_notes_page.dart';
import 'package:pushboy_web/widgets/network/change_route_page.dart';
import 'package:pushboy_web/widgets/network/edit_page.dart';
import 'package:pushboy_web/widgets/network/metrics_page.dart';
import 'package:pushboy_web/widgets/network/network_create_page.dart';
import 'package:pushboy_web/widgets/network/network_tokens_overview_page.dart';
import 'package:pushboy_web/widgets/network/overview_page.dart';

import 'package:pushboy_web/widgets/payment/add_card_page.dart';
import 'package:pushboy_web/widgets/payment/add_credit_page.dart';
import 'package:pushboy_web/widgets/payment/manage_autopay_page.dart';
import 'package:pushboy_web/widgets/payment/pay_form/checkout_confirm_page.dart';
import 'package:pushboy_web/widgets/payment/pay_form/checkout_crypto_page.dart';
import 'package:pushboy_web/widgets/payment/pay_form/checkout_stripe_page.dart';
import 'package:pushboy_web/widgets/payment/pay_form/thank_you_page.dart';
import 'package:pushboy_web/widgets/payment/pay_form/wait_for_stripe_page.dart';
import 'package:pushboy_web/widgets/payment/billing_activity_page.dart';

import 'package:pushboy_web/widgets/payment/bill_page.dart';
import 'package:pushboy_web/widgets/bill/pay_now.dart';
import 'package:pushboy_web/widgets/payment/view_bills_page.dart';
import 'package:pushboy_web/widgets/payment/view_payments_page.dart';

import 'package:pushboy_web/widgets/plan/change_page.dart';

import 'package:pushboy_web/widgets/project/alerts_tab_page.dart';
import 'package:pushboy_web/widgets/project/change_name_page.dart';
import 'package:pushboy_web/widgets/project/change_notes_page.dart';
import 'package:pushboy_web/widgets/project/metrics_tab_page.dart';
import 'package:pushboy_web/widgets/project/networks_tab_page.dart';
import 'package:pushboy_web/widgets/project/payments_tab_page.dart';
import 'package:pushboy_web/widgets/project/plan_tab_page.dart';
import 'package:pushboy_web/widgets/project/project_create_page.dart';
import 'package:pushboy_web/widgets/project/settings_tab_page.dart';

import 'package:pushboy_web/widgets/shared/image_upload_page.dart';
import 'package:pushboy_web/widgets/sign_in/sign_in_page.dart';
import 'package:pushboy_web/widgets/sign_in/confirm_email_page.dart';
import 'package:pushboy_web/widgets/sign_in/dev_email_confirm_link.dart';
import 'package:pushboy_web/widgets/sign_in/surfer_email_confirm_link.dart';

import 'package:pushboy_web/widgets/stat/change_body_page.dart';
import 'package:pushboy_web/widgets/stat/change_title_page.dart';
import 'package:pushboy_web/widgets/stat/change_url_page.dart';
import 'package:pushboy_web/widgets/stat/edit_page.dart';

Route<dynamic> route(BuildContext context, RouteSettings settings) {
  return MaterialPageRoute(
      settings: settings,
      builder: (c) {
        final args = getProcessedArgs(settings);
        final routeParams = getRouteParams(context, args: args);
        updateUrlParams(args);

        String name = settings.name ?? '';
        name = name.split('?')[0];
        if (name[0] == '/') {
          name = name.substring(1);
        }

        switch (name) {
          case Routes.empty:
          case Routes.home:
          case Routes.homeWord:
            return HomePage();
          case Routes.console:
            return const ConsolePage();
          case Routes.contact:
            return ContactPage();
          case Routes.confirmEmail:
            return ConfirmEmailPage();
          case Routes.developerEmailConfirmLink:
            return DevEmailConfirmLink(miscId: args.misc.id);
          case Routes.surferEmailConfirmLink:
            return SurferEmailConfirmLink(miscId: args.misc.id);
          case Routes.surferChangeEmailValidateNewLink:
            return SurferChangeEmailValidateNewLinkPage(
                requestChange: args.requestChange);

          case Routes.developerPrivacy:
            return const WebPolicyPage(
                audience: 'developer', policy: 'privacy');
          case Routes.developerTerms:
            return const WebPolicyPage(audience: 'developer', policy: 'terms');
          case Routes.surferPrivacy:
            return const MobilePolicyPage(
                audience: 'surfer', policy: 'privacy');

          case Routes.developer:
            return const DeveloperPage();
          case Routes.developerChangeEmailRequest:
            return DeveloperChangeEmailRequestPage(developer: args.developer);
          case Routes.developerChangeEmailLink:
            return DeveloperChangeEmailLinkPage(
                requestChange: args.requestChange);
          case Routes.developerChangeEmailValidateNewLink:
            return DeveloperChangeEmailValidateNewLinkPage(
                requestChange: args.requestChange);

          case Routes.developerChangePasswordRequest:
            return DeveloperChangePasswordRequestPage(
                developer: args.developer);
          case Routes.developerChangePasswordLink:
            return DeveloperChangePasswordLinkPage(
                requestChange: args.requestChange);
          case Routes.loggedOutDeveloperChangePasswordRequest:
            return LoggedOutDeveloperChangePasswordRequestPage();

          case Routes.developerChangePhone:
            return DeveloperChangePhonePage(developer: args.developer);

          case Routes.surferChangeEmailLink:
            return SurferChangeEmailLinkPage(requestChange: args.requestChange);
          case Routes.surferChangePasswordLink:
            return SurferChangePasswordLinkPage(
                requestChange: args.requestChange);

          case Routes.alerts:
            return const AlertsPage();
          case Routes.signIn:
            return SignInPage();

          case Routes.boiChangeChannel:
            return BoiChangeChannelPage(
                boiSender: args.boiSender, network: args.network);
          case Routes.boiChangeThumbUrl:
            return ImageUploadPage(boiSender: args.boiSender);
          case Routes.boiLayoutDescriptions:
            return BoiLayoutDescriptionsPage();
          case Routes.boiSend:
            return BoiSendPage(network: args.network);
          case Routes.boiStatus:
            return BoiStatusPage(boi: args.boi);

          case Routes.channelEdit:
            return ChannelEditPage(channel: args.channel);

          case Routes.channelChangeAbbrev:
            return ChannelChangeAbbrevPage(channel: args.channel);
          case Routes.channelChangeColorPrimary:
            return ChannelChangeColorPage(
                channel: args.channel, whichColor: WhichColor.primary);
          case Routes.channelChangeColorSecondary:
            return ChannelChangeColorPage(
                channel: args.channel, whichColor: WhichColor.secondary);
          case Routes.channelChangeDescription:
            return ChannelChangeDescriptionPage(channel: args.channel);
          case Routes.channelChangeName:
            return ChannelChangeNamePage(channel: args.channel);
          case Routes.channelChangeNotes:
            return ChannelChangeNotesPage(channel: args.channel);
          case Routes.channelChangeRoute:
            return ChannelChangeRoutePage(channel: args.channel);
          case Routes.channelChangeThumb:
            return ImageUploadPage(channel: args.channel);

          case Routes.channelCreate:
            return ChannelCreatePage(network: args.network);
          case Routes.channelOverview:
            return ChannelOverviewPage(channel: args.channel);
          case Routes.channelTokensOverview:
            return ChannelTokensOverviewPage(channel: args.channel);

          case Routes.networkCreate:
            return NetworkCreatePage(project: args.project);
          case Routes.networkEdit:
            return NetworkEditPage(network: args.network);

          case Routes.networkChangeAbbrev:
            return NetworkChangeAbbrevPage(network: args.network);
          case Routes.networkChangeColorPrimary:
            return NetworkChangeColorPage(
                network: args.network, whichColor: WhichColor.primary);
          case Routes.networkChangeColorSecondary:
            return NetworkChangeColorPage(
                network: args.network, whichColor: WhichColor.secondary);
          case Routes.networkChangeDescription:
            return NetworkChangeDescriptionPage(network: args.network);
          case Routes.networkChangeName:
            return NetworkChangeNamePage(network: args.network);
          case Routes.networkChangeNotes:
            return NetworkChangeNotesPage(network: args.network);
          case Routes.networkChangeRoute:
            return NetworkChangeRoutePage(network: args.network);
          case Routes.networkChangeThumb:
            return ImageUploadPage(network: args.network);

          case Routes.networkMetrics:
            return NetworkMetricsPage(network: args.network);
          case Routes.networkOverview:
            return NetworkOverviewPage(network: args.network);
          case Routes.networkTokensOverview:
            return NetworkTokensOverviewPage(network: args.network);

          case Routes.projectChangeName:
            return ProjectChangeNamePage(project: args.project);
          case Routes.projectChangeNotes:
            return ProjectChangeNotesPage(project: args.project);

          case Routes.planChange:
            return PlanChangePage(plan: args.plan, project: args.project);

          case Routes.paymentAddCard:
            return AddCardPage(project: args.project);
          case Routes.paymentAddCredit:
            return AddCreditPage(project: args.project);
          case Routes.paymentAutopay:
            return ManageAutopayPage(project: args.project);
          case Routes.paymentCheckoutConfirm:
            return CheckoutConfirmPage(
                paymentDetails: args.paymentDetails, project: args.project);
          case Routes.paymentCheckoutCrypto:
            return CheckoutCryptoPage(
                amount: args.amount, project: args.project);
          case Routes.paymentCheckoutStripe:
            return CheckoutStripePage(
                amount: args.amount, project: args.project);

          case Routes.billOverview:
            return BillPage(bill: args.bill, project: args.project);
          case Routes.billPayNow:
            return PayNowPage(bill: args.bill, project: args.project);
          case Routes.billingActivity:
            return BillingActivityPage(project: args.project);
          case Routes.billsView:
            return ViewBillsPage(project: args.project);

          case Routes.paymentsView:
            return ViewPaymentsPage(project: args.project);

          case Routes.paymentThankYou:
            return ThankYouPage(project: args.project);
          case Routes.paymentWaitForStripe:
            return WaitForStripePage(
              paymentAttemptId: args.paymentAttemptId,
              project: args.project,
              stripeUrl: args.stripeUrl,
            );

          case Routes.projectCreate:
            return ProjectCreatePage();
          case Routes.projectAlertsTab:
            return ProjectAlertsTabPage(project: args.project);
          case Routes.projectMetricsTab:
            return ProjectMetricsTabPage(project: args.project);
          case Routes.projectNetworksTab:
            return ProjectNetworksTabPage(project: args.project);
          case Routes.projectPaymentsTab:
            return ProjectPaymentsTabPage(project: args.project);
          case Routes.projectPlanTab:
            return ProjectPlanTabPage(project: args.project);
          case Routes.projectSettingsTab:
            return ProjectSettingsTabPage(project: args.project);

          case Routes.statChangeBody:
            return StatChangeBodyPage(channel: args.channel);
          case Routes.statChangeThumbUrl:
            return ImageUploadPage(channel: args.channel, editStatic: true);
          case Routes.statChangeTitle:
            return StatChangeTitlePage(channel: args.channel);
          case Routes.statChangeUrl:
            return StatChangeUrlPage(channel: args.channel);
          case Routes.statEdit:
            return StatEditPage(channel: args.channel);

          default:
            final msg = 'Invalid route: ${settings.name} from $name';
            print(msg);
            //throw Exception('Invalid route: ${settings.name} from $name');
            return HomePage();
        }
      });
}
